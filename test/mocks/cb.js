/* eslint-env jest */

const cb = {
  _callbacks: {
    onTip: null,
    onMessage: null,
    onEnter: null,
    onDrawPanel: null,
    tipOptions: null
  },

  tip (item) {
    this._callbacks.onTip.call(null, item)
  },

  message (item) {
    return this._callbacks.onMessage.call(null, item)
  },

  enter (entrance) {
    this._callbacks.onEnter.call(null, entrance)
  },

  drawPanel () {
    this._callbacks.onDrawPanel.call(null)
  },

  onTip (callback) {
    this._callbacks.onTip = callback
  },

  onMessage (callback) {
    this._callbacks.onMessage = callback
  },

  onEnter (callback) {
    this._callbacks.onEnter = callback
  },

  onDrawPanel (callback) {
    this._callbacks.onDrawPanel = callback
  },

  changeRoomSubject: jest.fn(),

  sendNotice: jest.fn(),

  setTimeout: (fn, timeout) => {
    setTimeout(fn, timeout)
  },

  settings: {
    theme: 'Dungeons & Dragons',
    goal_amount: 1000,
    level_up_amount: 200,
    prize_rolling: 'on',
    attack_cost: 44,
    multi_roll: 'no',
    show_avatars: 'yes',
    prize_1: '<prize.1>',
    prize_2: '<prize.2>',
    prize_3: '<prize.3>',
    prize_4: '<prize.4>',
    prize_5: '<prize.5>',
    bonus_charm_prize: '<charm>',
    bonus_domination_prize: '<domination>',
    bonus_flash_prize: '<flash>'
  },

  room_slug: '<broadcaster>'
}

module.exports = () => Object.assign({}, cb)
