/**
 * @ts-check
 * @overview Parses and fires callbacks in response to chat commands
 */

'use strict'

import assert from 'assert'

const ERROR_INVALID_PATTERN = 'Invalid `pattern` argument for Command#register'
const ERROR_INVALID_PARAMS = 'Invalid `params` argument for Command#register'

const PATTERNS = new Map()
  .set('any', /.*/)
  .set('directive', /^\w+/)
  .set('string', /(?!^").+(?=")/)
  .set('assignment', /(?!^\w+\s*=\s*)\w+$/)
  .set('number', /^\d+/)

function getArguments (text, command, exactMatching) {
  const params = command.params
  const args = {}
  let subtext

  if (exactMatching) {
    subtext = text.trim().replace(RegExp(`^${command.pattern}\\s*`), '')
  } else {
    subtext = text.trim().replace(RegExp(`${command.pattern}\\s*`), '')
  }

  for (const param of params) {
    const typePattern = PATTERNS.get(param.type)

    if (typePattern.test(subtext)) {
      const value = subtext.match(typePattern)[0]
      args[param.name] = !param.values.length || ~param.values.map(v => v.toLowerCase()).indexOf(value.toLowerCase())
        ? value
        : null
    } else {
      args[param.name] = null
    }
  }

  return args
}

function hasCommand (text, command, exactMatching) {
  const pattern = exactMatching ? `^${command.pattern}\\b\\s*` : `${command.pattern}\\b\\s*`
  return RegExp(pattern).test(text)
}

class Command {
  /**
   * @param {Object} options Command eval options
   * @param {boolean} options.exactMatching Whether or not inputs must strictly match command patterns
   */
  constructor (options = { exactMatching: true }) {
    /** @type {Map} */
    this.registry = new Map()

    /** @type {boolean} */
    this.exactMatching = options.exactMatching
  }

  evaluate (message) {
    let result = null

    for (const command of this.registry.values()) {
      if (hasCommand(message.m, command, this.exactMatching)) {
        const args = command.params ? getArguments(message.m, command, this.exactMatching) : null
        result = command.callback(message, command, args)

        break
      }
    }

    return result
  }

  register (pattern, params, callback) {
    assert(pattern.constructor === String, `${ERROR_INVALID_PATTERN}: ${JSON.stringify(pattern)}`)

    if (params) {
      assert(params.constructor === Array, `${ERROR_INVALID_PARAMS}: ${JSON.stringify(params)}`)
    }

    this.registry.set(pattern, {
      pattern,
      params,
      callback
    })

    return this
  }
}

export {
  Command as default,
  ERROR_INVALID_PATTERN,
  ERROR_INVALID_PARAMS
}
