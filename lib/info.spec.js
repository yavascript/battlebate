/* eslint-env jest */

'use strict'

import Info, {
  showHeroes,
  showHelp,
  showPrizes,
  showTLDR
} from './info'

import Player from './player'

describe('Info', () => {
  let messenger
  let game
  let graphics
  let info

  beforeEach(() => {
    messenger = {
      whisper: jest.fn(),
      say: jest.fn()
    }

    game = {
      announcements: {
        hint: jest.fn()
      },
      cb: {
        settings: {
          bonus_charm_prize: '<prize.charm>',
          bonus_flash_prize: '<prize.flash',
          bonus_domination_prize: '<prize.domination>',
          level_up_amount: 100
        }
      },
      prizes: [],
      winners: []
    }

    graphics = {
      render: jest.fn()
    }

    info = new Info(messenger, game, graphics)
  })

  describe('initialize', () => {
    it('should inject the `messenger` dependency', () => (
      expect(info.messenger).toEqual(messenger)
    ))

    it('should inject the `game` dependency', () => (
      expect(info.game).toEqual(game)
    ))

    it('should inject the `graphics` dependency', () => (
      expect(info.graphics).toEqual(graphics)
    ))
  })

  describe.each([
    { progress: 0, total: 0, remaining: 100, bar: '[----------]' },
    { progress: 50, total: 50, remaining: 50, bar: '[+++++-----]' },
    { progress: 0, total: 100, remaining: 100, bar: '[----------]' },
    { progress: 50, total: 150, remaining: 50, bar: '[+++++-----]' }
  ])('#showXP', ({ bar, progress, remaining, total }) => {
    beforeEach(() => {
      const player = new Player('<name>')
      player.xp = total

      Player.configure('LEVEL_UP_COST', 100)
      player.changeHero('fighter')
      info.showXP(player)
    })

    it('should display an XP bar', () => {
      expect(messenger.whisper).toHaveBeenCalledWith(`${progress} / 100 XP :: Tip ${remaining} more to win [random prize]`, '<name>')
    })
  })

  describe('#showPrizes', () => {
    describe('when given a username', () => {
      beforeEach(() => {
        info.showPrizes({ name: '<name>' })
      })

      it('should send the message to the specified user', () => (
        expect(messenger.whisper).toHaveBeenCalledWith(showPrizes(info.game), '<name>')
      ))
    })

    describe('when not given a username', () => {
      beforeEach(() => {
        info.showPrizes()
      })

      it('should send the message to general chat', () => (
        expect(messenger.say).toHaveBeenCalledWith(showPrizes(info.game))
      ))
    })
  })

  describe('#showHeroes', () => {
    describe('when given a username', () => {
      beforeEach(() => {
        info.showHeroes({ name: '<name>' })
      })

      it('should send the message to the specified user', () => (
        expect(messenger.whisper).toHaveBeenCalledWith(showHeroes(info.game, info.graphics), '<name>')
      ))
    })

    describe('when not given a username', () => {
      beforeEach(() => {
        info.showHeroes()
      })

      it('should send the message to chat', () => (
        expect(messenger.say).toHaveBeenCalledWith(showHeroes(info.game, info.graphics))
      ))
    })
  })

  describe('#showTLDR', () => {
    describe('when given a username', () => {
      beforeEach(() => {
        info.showTLDR({ name: '<name>' })
      })

      it('should send the message to the user', () => (
        expect(messenger.whisper).toHaveBeenCalledWith(showTLDR(info.game, info.graphics), '<name>')
      ))
    })

    describe('when not given a username', () => {
      beforeEach(() => {
        info.showTLDR()
      })

      it('should send the message to general chat', () => (
        expect(messenger.say).toHaveBeenCalledWith(showTLDR(info.game, info.graphics))
      ))
    })
  })

  describe('#showHelp', () => {
    beforeEach(() => {
      info.showHelp({ name: '<name>' })
    })

    it('should show the given user the help menu', () => (
      expect(messenger.whisper).toHaveBeenCalledWith(showHelp(info.game, info.graphics), '<name>')
    ))
  })
})
