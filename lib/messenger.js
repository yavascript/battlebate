/**
 * @overview Sends chat messages and announcements
 */

'use strict'

import Graphics from './graphics'

const colors = new Map()
  .set('say', '#9B59B6')
  .set('whisper', '#006080')
  .set('reward', '#D43900')
  .set('attack', '#696969')
  .set('level-up', '#1E824C')
  .set('taunt', '#B50000')

class Messenger {
  /**
   * @param {Object} cb - Chaturbate interface
   */
  constructor (cb) {
    this.cb = cb
  }

  /**
   * @param {string} message - Message text
   * @param {Object} options - Output options
   * @param {string} options.user - Username of the message recipient
   * @param {string} optoins.group - Group name of the message recipients
   * @param {string} options.color - Foreground color of the message
   * @returns {Messenger} `Messenger` instance
   */
  send (message, options = { user: '', group: '', color: '' }) {
    this.cb.sendNotice(message.trim(), options.user, null, options.color, 'bold', options.group)

    return this
  }

  /**
   * Send a message to all
   * @param {string} message - Message text
   * @returns {Messenger} `Messenger` instance
   */
  say (message) {
    const color = colors.get('say')
    this.send(message, { color })

    return this
  }

  /**
   * Send message to a specific user
   * @param {string} message - Message text
   * @param {string} user - Username of the message recipient
   * @returns {Messenger} `Messenger` instance
   */
  whisper (message, user) {
    const color = colors.get('whisper')
    this.send(message, { user, color })

    return this
  }

  /**
   * Send a message to all, for prize winnings
   * @param {Payer} player - Message recipient
   * @param {string} prize - Prize to be awarded
   * @param {string} die - Die number of the roll
   * @returns {Messenger} `Messenger` instance
   */
  reward (player, prize, die) {
    const graphic = die ? Graphics.render(`die-${die}`) : Graphics.render('die-bonus')
    const color = colors.get('reward')
    const message = `${graphic} :: ${player.name} has won: --- ${prize}! ---`

    this.send(message, { color })

    return this
  }

  /**
   * Send a message to all, for when a player levels up
   * @param {Player} player - Player that's leveling up
   * @returns {Messenger} `Messenger` instance
   */
  levelUp (player) {
    const graphic = Graphics.render('level-up')
    const color = colors.get('level-up')

    this.send(`${graphic} :: ${player.name} reached Level ${player.level}!`, { color })

    return this
  }

  /**
   * Send a message to all, for when a player levels up
   * @param {Enemy} enemy - Taunting game enemy
   * @param {string} message - Message text
   * @returns {Messenger} `Messenger` instance
   */
  taunt (enemy, message) {
    const graphic = Graphics.render(enemy.graphic.emoji)
    const color = colors.get('taunt')

    this.send(`${graphic} :: ${message || enemy.taunt()}`, { color })

    return this
  }
}

export { colors }
export default Messenger
