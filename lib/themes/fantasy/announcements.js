/**
 * @overview Fantasy themed announcements
 */

'use strict'

import startCase from 'lodash/startCase'

class Announcements {
  static spawn (enemy) {
    return `The ${startCase(enemy.name)} appears!`
  }

  static defeat (player, enemy) {
    return `${player.name} has slain the ${startCase(enemy.name)}!`
  }

  static heroChange (player, cb) {
    const levelUpPrize = cb.settings[player.hero.bonusPrize] || 'random prize'
    const levelUpAmount = cb.settings.level_up_amount

    return `You are a ${startCase(player.hero.name)} - every ${levelUpAmount} tokens tipped wins you: [${levelUpPrize}]! :: Type /hero for more`
  }

  static hint () {
    return `*** Only your tokens can defeat the monster! ***`
  }
}

export default Announcements
