
/**
 * @overview Fantasy setting enemies
 */

'use strict'

import UUIDs, { UUID_LIZARD, UUID_OGRE, UUID_ORC_WARRIOR, UUID_SKELETON_ARCHER, UUID_TROLL, UUID_DRAUGR, UUID_SKELETON_WARRIOR, UUID_ANCIENT_LICH } from '../../uuids'

const taunts = new Map()
  .set('ancient-lich', [
    `Just as planned...`,
    `Good...all goes according to plan...`,
    `Look at them...so ripe, so innocent...`,
    `Your souls are mine!`,
    `Excellent, more souls!`,
    `The ancient evil survives!`,
    `I love it when a plan comes together...`,
    `This is no dress! It's the standard Lich uniform...`,
    `I should've printed more brochures for the cult...`,
    `The path of damned is eternal...and you get dental!`,
    `Don't worry about the fine print, the soul consumption clause is never exercised...`
  ])
  .set('draugr', [
    `I have awoken.`,
    `I have returned.`,
    `Revenge...`,
    `Death is not the end...`,
    `Death rages!`,
    `My patience has ended!`,
    `Ugh, I'm getting too old for this...`,
    `I think I'm having a mid-death crisis...`,
    `I'd rather be slumbering...`,
    `I'll be back...`
  ])
  .set('troll', [
    `I'm da strongest and da meanest there is!`,
    `I'll crush ya!`,
    `Gonna clobber da whole lot of ya!`,
    `Time to bust skulls!`,
    `Lemme at 'em!`,
    `Stand still so I can kill ya!`,
    `If it's a FIGHT ya want...`,
    `You call dat fightin?!`,
    `This is gonna HURT...a LOT!`
  ])
  .set('ogre', [
    `Gonna squish ya!`,
    `Gonna clobber ya REAL good!`,
    `Oh, dis is gonna be GOOD!`,
    `Now I'm hungry!`,
    `So...angry!`,
    `What you lookin at?!`,
    `Hit 'em with a rock!`,
    `Now I'm REAL mad!`,
    `Crush da runts!`,
    `Stomp 'em flat!`,
    `Lousy runts!`,
    `Squash da little ones!`,
    `Big is best!`
  ])
  .set('lizard-folk', [
    `Get 'em!`,
    `Chop 'em up!`,
    `Stick 'em with the pointy end!`,
    `Chaaarge!`,
    `Ready for fightin'!`,
    `Crush 'em!`,
    `It's ours for the takin'!`,
    `Swarm them!`,
    `'Ere we go!`
  ])
  .set('orc-warrior', [
    `The hunt begins!`,
    `You call this a fight?!`,
    `This is gonna be a REAL fight!`,
    `Now you're fightin' a REAL warrior.`,
    `Now's the time for tearin' things up!`,
    `Come here so I can squash ya, git!`,
    `Oh you're gonna PAY for that!`,
    `Orcs are MADE for fightin!`
  ])
  .set('skeleton', [
    `In the grave, no one can hear you scream. Well they can, but it's really muffled.`,
    `The damned have returned!`,
    `We have returned.`,
    `Lucky creatures. At long last you have found the tranquility of death...`,
    `So much fear. So much noise.`,
    `You will all join the army of the dead!`,
    `Join us!`
  ])
  .set('skeleton-archer', [
    `Shot through the heart, and I'm to blame.`,
    `All I see is blackness...oh, my hood's down.`
  ])
  .set('myrmidon', [
    `Our tide...is rising.`,
    `The tide turns...`,
    `Time is short, surface-dwellers.`,
    `I shall drown your sorrows.`,
    `This world will be ours, and we'll start with the swimming pools.`
  ])

/**
 * Fantasy genre enemies
 * @type {Map}
 */

const Enemies = [
  {
    name: 'lizardman',
    graphic: { id: UUID_LIZARD, x: 13, y: 17, emoji: 'lizardman' },
    taunts: taunts.get('lizard-folk')
  },

  {
    name: 'ogre',
    graphic: { id: UUID_OGRE, x: 10, y: 11, emoji: 'ogre' },
    taunts: taunts.get('ogre')
  },

  {
    name: 'orc warrior',
    graphic: { id: UUID_ORC_WARRIOR, x: 17, y: 10, emoji: 'orc-hellbringer' },
    taunts: taunts.get('orc-warrior')
  },

  {
    name: 'skeleton archer',
    graphic: { id: UUID_SKELETON_ARCHER, x: 11, y: 13, emoji: 'skeleton-archer' },
    taunts: taunts.get('skeleton').concat(taunts.get('skeleton-archer'))
  },

  {
    name: 'troll',
    graphic: { id: UUID_TROLL, x: 13, y: 8, emoji: 'troll' },
    taunts: taunts.get('troll')
  },

  {
    name: 'draugr',
    graphic: { id: UUID_DRAUGR, x: 9, y: 9, emoji: 'draugr' },
    taunts: taunts.get('draugr')
  },

  {
    name: 'skeleton-warrior',
    graphic: { id: UUID_SKELETON_WARRIOR, x: 10, y: 15, emoji: 'skeleton-swordsman' },
    taunts: taunts.get('skeleton')
  },

  {
    name: 'ancient-lich',
    graphic: { id: UUID_ANCIENT_LICH, x: 9, y: 8, emoji: 'lich' },
    taunts: taunts.get('ancient-lich')
  }
]

export default Enemies
