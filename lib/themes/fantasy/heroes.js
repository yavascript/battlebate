/**
 * @overview Fantasy setting heroes
 */

'use strict'

const heroes = new Map()

heroes.set('fighter', {
  name: 'fighter',
  bonusPrize: null,
  description: 'A master of combat',
  graphics: new Map()
    .set('novice', 'fighter-novice')
    .set('elite', 'fighter-elite')
    .set('legendary', 'fighter-legendary'),
  attackPhrases: [
    'slashes at the',
    'shield-bashes the',
    'attacks the',
    'charges at the'
  ],
  levelUpPhrase: 'heroic bravery'
})

heroes.set('bard', {
  name: 'bard',
  bonusPrize: 'bonus_charm_prize',
  description: 'The magical performance artist',
  graphics: new Map()
    .set('novice', 'bard-novice')
    .set('elite', 'bard-elite')
    .set('legendary', 'bard-legendary'),
  attackPhrases: [
    'thrusts at the',
    'shouts a word of power at the',
    'uses magic against the',
    'ripostes against the'
  ],
  levelUpPhrase: 'magical skill'
})

heroes.set('warlock', {
  name: 'warlock',
  bonusPrize: 'bonus_domination_prize',
  description: 'A wielder of dark and unnatural power',
  graphics: new Map()
    .set('novice', 'warlock-novice')
    .set('elite', 'warlock-elite')
    .set('legendary', 'warlock-legendary'),
  attackPhrases: [
    'unleashes eldrich energy against the',
    'summons dark magical tendrils to assault the',
    'unleashes a burst of hellfire against the',
    'channels telekinetic force to repel the'
  ],
  levelUpPhrase: 'dark pact'
})

heroes.set('ranger', {
  name: 'ranger',
  bonusPrize: 'bonus_flash_prize',
  description: 'The precision hunter and wielder of nature magic',
  graphics: new Map()
    .set('novice', 'ranger-novice')
    .set('elite', 'ranger-elite')
    .set('legendary', 'ranger-legendary'),
  attackPhrases: [
    'takes aim and strikes',
    'fires an arrow at',
    'looses a hailstorm of arrows at',
    'summons an elemental host to assault'

  ],
  levelUpPhrase: 'keen expertise'
})

export default heroes
