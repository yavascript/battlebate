/**
 * @overview Provides thematic flavor text/labels/values
 */

export { default as FantasyHeroes } from './fantasy/heroes'
export { default as FantasyEnemies } from './fantasy/enemies'
export { default as FantasyAnnouncements } from './fantasy/announcements'

export { default as BatmanHeroes } from './batman/heroes'
export { default as BatmanEnemies } from './batman/enemies'
export { default as BatmanAnnouncements } from './batman/announcements'
