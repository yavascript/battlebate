/**
 * @overview Batman themed announcements
 */

'use strict'

import startCase from 'lodash/startCase'

class Announcements {
  static spawn (enemy) {
    return `${startCase(enemy.name)} is wreaking havoc!`
  }

  static defeat (player, enemy) {
    return `${player.name} has taken down ${startCase(enemy.name)}!`
  }

  static heroChange (player, cb) {
    const levelUpPrize = cb.settings[player.hero.bonusPrize] || 'random prize'
    const levelUpAmount = cb.settings.level_up_amount

    return `You are ${startCase(player.hero.name)} - every ${levelUpAmount} tokens tipped wins you: [${levelUpPrize}]! :: Type /hero for more`
  }

  static hint () {
    return `*** Only your tokens can defeat the villain! ***`
  }
}

export default Announcements
