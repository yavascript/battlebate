/**
 * @overview Batman setting enemies
 */

'use strict'

import UUIDs, { UUID_JOKER, UUID_DEATHSTROKE, UUID_BANE } from '../../uuids'

const taunts = new Map()
  .set('joker', [
    `Aww, gotta say, I thought you'd have more fight in you! A LOT more fight!`,
    `If I weren't crazy, I'd be insane!`,
    `All it takes is one bad day to reduce the sanest man alive to lunacy.`,
    `Oh, you filthy degenerates!`,
    `So many happy faces!`,
    `What a rush!`,
    `You really should loosen up dear, have a laugh now and then!`,
    `Step right up and explore the insanity!`,
    `Don't worry, I promise a fair fight.`,
    `See, I'm not a monster; I'm just ahead of the curve...`,
    `Whatever doesn't kill you simply makes you stranger!`,
    `Madness is like gravity, all it takes is a little push!`,
    `They don't make straitjackets like they used to!`,
    `Guys! I just want to talk... About shooting you... With this gun... Which I'll do once we're done talking!`
  ])
  .set('deathstroke', [
    `I'm the best.`,
    `This fight is boring me.`,
    `I love my job...`,
    `Do you prefer steel or lead?`,
    `Still wanna be a hero? Go ahead, I dare ya.`,
    `It's been a long rivalry, Bat. But this is the only way it could end.`,
    `I'm not impressed.`,
    `Too easy. Your tactics are one-dimensional.`
  ])
  .set('bane', [
    `I am Bane - and I could kill you... but death would only end your agony - and silence your shame.`,
    `I am Bane - the last opponent you'll ever face!`,
    `You are beaten! Now I will break you!`,
    `Toys. You try to fight me with pathetic little toys.`,
    `You've got nothing!`,
    `Beg for mercy!`,
    `Scream my name!`,
    `Only when I'm dead do I intend to rest...`
  ])

/**
 * Batman villains
 * @type {Set}
 */
const Enemies = [
  {
    name: 'The Joker',
    graphic: { id: UUID_JOKER, x: 14, y: 13, emoji: 'joker' },
    taunts: taunts.get('joker')
  },

  {
    name: 'Deathstroke',
    graphic: { id: UUID_DEATHSTROKE, x: 7, y: 15, emoji: 'deathstroke' },
    taunts: taunts.get('deathstroke')
  },

  {
    name: 'Bane',
    graphic: { id: UUID_BANE, x: 8, y: 16, emoji: 'bane' },
    taunts: taunts.get('bane')
  }
]

export default Enemies
