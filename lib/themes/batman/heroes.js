/**
 * @overview Batman setting heroes
 */

'use strict'

const heroes = new Map()

heroes.set('batman', {
  name: 'batman',
  bonusPrize: null,
  description: 'Genius detective, trained to be the ultimate crime fighter',
  graphics: new Map()
    .set('novice', 'batman-novice')
    .set('elite', 'batman-elite')
    .set('legendary', 'batman-legendary'),
  attackPhrases: [
    'throws a batarang at',
    'uppercuts',
    'drives a knee into',
    'lands a punch on',
    'launches tear gas pellets at'
  ],
  levelUpPhrase: 'brutal justice'

})

heroes.set('alfred', {
  name: 'alfred',
  bonusPrize: 'bonus_charm_prize',
  description: `Ex special forces turned butler of Bruce Wayne. British.`,
  graphics: new Map()
    .set('novice', 'alfred-novice')
    .set('elite', 'alfred-elite')
    .set('legendary', 'alfred-legendary'),
  attackPhrases: [
    'throws a jab at',
    'engages in fisticuffs with',
    'lands a good wallop on',
    'uses their shotgun to bludgeon'
  ],
  levelUpPhrase: 'impeccable manners'
})

heroes.set('azrael', {
  name: 'azrael',
  bonusPrize: 'bonus_domination_prize',
  description: 'Enhanced assassin trained by a secretive religious order.',
  graphics: new Map()
    .set('novice', 'azrael-novice')
    .set('elite', 'azrael-elite')
    .set('legendary', 'azrael-legendary'),
  attackPhrases: [
    'drop kicks',
    'uses the pommel of his sword to batter',
    'brings both fists crashing down on',
    'body slams'
  ],
  levelUpPhrase: 'zealous crusade'
})

heroes.set('nightwing', {
  name: 'nightwing',
  bonusPrize: 'bonus_flash_prize',
  description: 'Dashing, acrobatic crime fighter.',
  graphics: new Map()
    .set('novice', 'nightwing-novice')
    .set('elite', 'nightwing-elite')
    .set('legendary', 'nightwing-legendary'),
  attackPhrases: [
    'uses their escrima sticks against',
    'throws a cluster of stun pellets at',
    'sweeps the leg of',
    'jump kicks'
  ],
  levelUpPhrase: 'acrobatic prowess'
})

export default heroes
