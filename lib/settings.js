/**
 * @overview Manage and read game settings
 */

class Settings {
  constructor (cb) {
    this.cb = cb
    this.entries = new Map()
  }

  options () {
    return [
    ]
  }
}

export default Settings
