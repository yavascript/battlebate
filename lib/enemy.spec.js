/* eslint-env jest */

'use strict'

jest.mock('lodash/shuffle', () => (arr) => arr)

import Enemy from './enemy'

import { BatmanEnemies, FantasyEnemies } from './themes'

function initialize () {
  const enemy = new Enemy({ hp: 1000 })

  it('should return an instance of `Enemy`', () => {
    expect(enemy).toBeInstanceOf(Enemy)
  })

  it('should assign enemy properties', () => {
    expect(enemy.hp).toBe(1000)
  })
}

function damage () {
  describe('when the damage does not exceed current hp', () => {
    const enemy = new Enemy({ hp: 1000 })
    enemy.damage(10)

    it('should reduce the total HP', () => {
      expect(enemy.hp).toBe(990)
    })
  })

  describe('when the damage exceeds current hp', () => {
    const enemy = new Enemy({ hp: 1000 })
    enemy.damage(1000)

    it('should reset damage to zero', () => {
      expect(enemy.hp).toBe(0)
    })
  })
}

function taunt () {
  const enemy = new Enemy({ hp: 1000 })

  it.each(enemy.persona.taunts)('should return the next taunt', (text) => {
    expect(enemy.taunt()).toBe(text)
  })

  it('should reset after iterating through taunts', () => {
    expect(enemy.taunt()).toBe(enemy.persona.taunts[0])
  })

  it('should continue iteration after resetting', () => {
    expect(enemy.taunt()).toBe(enemy.persona.taunts[1])
  })
}

describe('Enemy', () => {
  describe('when the Fantasy theme is set', () => {
    beforeAll(() => {
      Enemy.setTheme('fantasy')
    })

    describe('initialize', initialize)
    describe('#damage', damage)
    describe('#taunt', taunt)

    describe.each(FantasyEnemies.concat(FantasyEnemies))('attributes', ({ name, graphic, taunts }) => {
      let enemy

      beforeEach(() => { enemy = new Enemy({ hp: 500 }) })

      it('should be a monster', () => {
        expect(enemy.level).toBe(3)
        expect(enemy.name).toBe(name)
        expect(enemy.graphic).toBe(graphic)
        expect(enemy.taunts).toEqual(new Set(taunts).values())
      })
    })
  })

  describe('when the Batman theme is set', () => {
    beforeAll(() => {
      Enemy.setTheme('batman')
    })

    describe('initialize', initialize)
    describe('#damage', damage)

    describe.each(BatmanEnemies)('attributes', ({ name, graphic, taunts }) => {
      let enemy

      beforeEach(() => { enemy = new Enemy({ hp: 50 }) })

      it('should be a villain', () => {
        expect(enemy.level).toBe(1)
        expect(enemy.name).toBe(name)
        expect(enemy.graphic).toBe(graphic)
        expect(enemy.taunts).toEqual(new Set(taunts).values())
      })
    })
  })
})
