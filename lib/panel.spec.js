/* eslint-env jest */

import Panel from './panel'
import mockCB from '../test/mocks/cb'

describe('Panel', () => {
  describe.each([
    { hp: 100, maxHP: 100, value: 61 },
    { hp: 50, maxHP: 100, value: -33 },
    { hp: 1, maxHP: 100, value: -125.12 }
  ])('.healthOffset', ({ hp, maxHP, value }) => {
    let cb, game, panel

    beforeEach(() => {
      cb = mockCB()
      game = { enemy: { hp, maxHP } }
      panel = new Panel(cb, game)
    })

    it('should represent a percentage of health remaining', () => (
      expect(panel.healthOffset).toBe(value)
    ))
  })
})
