/* eslint-env jest */

'use strict'

jest.useFakeTimers()

import startCase from 'lodash/startCase'

import Game from './game'
import Player from './player'
import mockCB from '../test/mocks/cb'

global.MODE = 'main'
global.ENV = 'production'

const suite = (theme) => {
  describe('Game', () => {
    let cb
    let game

    beforeEach(() => {
      cb = mockCB()
      cb.settings.show_avatars = 'yes'

      game = new Game(cb).start()

      if (theme) {
        game.setTheme(theme)
      }

      jest.spyOn(game, 'logWinning')
      jest.spyOn(game.info, 'showXP')
      jest.spyOn(game.messenger, 'levelUp')
      jest.spyOn(game.messenger, 'reward')
      jest.spyOn(game.messenger, 'say')
      jest.spyOn(game.messenger, 'taunt')
      jest.spyOn(game.messenger, 'whisper')
    })

    describe('#addPlayer', () => {
      let player

      beforeEach(() => {
        player = '<user>'
        game.addPlayer(player)
      })

      it('should add the given player to the party', () => (
        expect(game.players.has(player)).toBe(true)
      ))
    })

    describe('#setupPrizes', () => {
      beforeEach(() => {
        game.setupPrizes()
      })

      it('should assign prizes', () => (
        expect(game.prizes).toEqual([
          '<prize.1>',
          '<prize.2>',
          '<prize.3>',
          '<prize.4>',
          '<prize.5>'
        ])
      ))
    })

    describe('#spawnEnemy', () => {
      let previousEnemy
      let announcement

      beforeEach(() => {
        previousEnemy = game.enemy
        game.spawnEnemy(500)

        if (theme === 'Batman') {
          announcement = `${startCase(game.enemy.name)} is wreaking havoc!`
        } else {
          announcement = `The ${startCase(game.enemy.name)} appears!`
        }
      })

      it('should replace the current enemy instance', () => (
        expect(game.enemy).not.toBe(previousEnemy)
      ))

      it('should announce the new enemy', () => (
        expect(game.messenger.say).toHaveBeenCalledWith(announcement)
      ))

      it('should assign a hitpoints value', () => (
        expect(game.enemy.hp).toBe(cb.settings.goal_amount)
      ))
    })

    describe('#reward', () => {
      const player = new Player('<user>')

      beforeEach(() => {
        game.logWinning(player, '<prize>')
      })

      it('should log a given prize winning', () => {
        expect(game.winners.length).toBe(1)
        expect(game.winners[0].player).toEqual(player)
        expect(game.winners[0].prize).toBe('<prize>')
      })
    })

    describe('when the game starts', () => {
      beforeEach(() => {
        game = new Game(cb).start()

        // jest.spyOn(game.info, 'showHeroes')
        jest.spyOn(game.info, 'showPrizes')
        jest.spyOn(game.info, 'showWinners')
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 44, message: '' })
        jest.advanceTimersByTime(600000)
      })

      it('should set a reminder to broadcast prize info', () => (
        expect(game.info.showPrizes).toHaveBeenCalledTimes(1)
      ))

      it('should set a reminder to broadcast winner info', () => (
        expect(game.info.showWinners).toHaveBeenCalledTimes(2)
      ))
    })

    describe('when any tip is received', () => {
      let player

      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 1, message: '' })

        player = game.players.get('<user>')
      })

      it('should show the player their XP bar', () => (
        expect(game.info.showXP).toHaveBeenCalledWith(player)
      ))
    })

    describe('when an attack roll tip is received', () => {
      let attack
      let player

      beforeEach(() => {
        const entrance = { user: '<user>' }
        const tip = { from_user: '<user>', amount: 44, message: '' }

        cb.enter(entrance)

        player = game.players.get('<user>')
        game.cb.settings.prize_rolling = 'on'
        game.prizes = ['<prize>']
        attack = jest.spyOn(game.players.get('<user>'), 'attack')
        cb.tip(tip)
      })

      it('should trigger an attack from the player', () => (
        expect(attack).toHaveBeenCalledWith(game.enemy, 44)
      ))

      it('should reward a prize', () => {
        expect(game.logWinning).toHaveBeenCalled()
        expect(game.messenger.reward).toHaveBeenCalledWith(player, '<prize>', 1)
      })

      it('should add the amount to `totalDamage`', () => (
        expect(game.totalDamage).toBe(44)
      ))
    })

    describe('when an attack roll tip is received (prize rolling disabled)', () => {
      let attack

      beforeEach(() => {
        const entrance = { user: '<user>' }
        const tip = { from_user: '<user>', amount: 44, message: '' }

        cb.enter(entrance)
        game.cb.settings.prize_rolling = 'off'
        attack = jest.spyOn(game.players.get('<user>'), 'attack')
        cb.tip(tip)
      })

      it('should trigger an attack from the player', () => (
        expect(attack).toHaveBeenCalled()
      ))

      it('should not reward a prize', () => {
        expect(game.logWinning).not.toHaveBeenCalled()
        expect(game.messenger.reward).not.toHaveBeenCalled()
      })

      it('should add the amount to `totalDamage`', () => (
        expect(game.totalDamage).toBe(44)
      ))
    })

    describe('when a large attack roll tip is received (multi-roll enabled)', () => {
      let attack

      beforeEach(() => {
        const entrance = { user: '<user>' }
        const tip = { from_user: '<user>', amount: 88, message: '' }

        cb.enter(entrance)
        attack = jest.spyOn(game.players.get('<user>'), 'attack')
        cb.tip(tip)
      })

      it('should trigger an attack from the player', () => (
        expect(attack).toHaveBeenCalledWith(game.enemy, 88)
      ))

      it('should add the amount to `totalDamage`', () => (
        expect(game.totalDamage).toBe(88)
      ))
    })

    describe('when a large attack roll tip is received (multi-roll disabled)', () => {
      let attack

      beforeEach(() => {
        const entrance = { user: '<user>' }
        const tip = { from_user: '<user>', amount: 88, message: '' }

        cb.settings.multi_roll = 'At most once per tip'
        cb.enter(entrance)
        attack = jest.spyOn(game.players.get('<user>'), 'attack')
        cb.tip(tip)
      })

      it('should trigger an attack from the player', () => (
        expect(attack).toHaveBeenCalledWith(game.enemy, 88)
      ))

      it('should add the amount to `totalDamage`', () => (
        expect(game.totalDamage).toBe(88)
      ))
    })

    describe('when a tip is high enough to insta-kill an enemy', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 1000, message: '' })
      })

      it('should spawn a new enemy', () => (
        expect(game.enemy.hp).toBe(1000)
      ))
    })

    describe('when a tip is overkill', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 1500, message: '' })
      })

      it('should carry over to the next enemy', () => (
        expect(game.enemy.hp).toBe(500)
      ))

      it('should carry over across an enemy life', () => {
        expect(game.totalKills).toBe(1)
        expect(game.enemy.hp).toBe(500)
      })
    })

    describe('when a tip is high enough to overkill multiple enemies', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 3250, message: '' })
      })

      it('should carry over across multiple enemy lives', () => {
        expect(game.totalKills).toBe(3)
        expect(game.enemy.hp).toBe(750)
      })
    })

    describe('when a tip is high enough to trigger a single Level Up', () => {
      let player

      beforeEach(() => {
        cb.enter({ user: '<user>' })

        player = game.players.get('<user>')
        player.hero.bonusPrize = null
        game.prizes = ['<prize>']

        cb.tip({ from_user: '<user>', amount: 200, message: '' })
      })

      it('should announce the Level Up', () => {
        expect(game.messenger.levelUp).toHaveBeenCalledWith(player)
        expect(game.messenger.reward).toHaveBeenCalledWith(player, '<prize>')
      })
    })

    describe('when a tip is high enough to trigger multiple Level Ups', () => {
      let player

      beforeEach(() => {
        cb.enter({ user: '<user>' })

        player = game.players.get('<user>')
        player.hero.bonusPrize = null
        game.prizes = ['<prize>']

        cb.tip({ from_user: '<user>', amount: 400, message: '' })
      })

      it('should announce the Level Ups', () => {
        expect(game.messenger.levelUp).toHaveBeenCalledWith(player)
        expect(game.messenger.reward).toHaveBeenCalledWith(player, '<prize>')
      })
    })

    describe('when a message is received', () => {
      beforeEach(() => {
        jest.spyOn(cb._callbacks, 'onMessage')
        cb.message({ user: '<user>', m: '<message>' })
      })

      it('should fire the `onMessage` callback', () => (
        expect(cb._callbacks.onMessage).toHaveBeenCalled()
      ))
    })

    describe('when a message is received that is not a command', () => {
      let exampleHero
      let message

      beforeEach(() => {
        if (theme === 'Batman') {
          exampleHero = {
            name: 'batman',
            label: 'Batman',
            icon: ':battlebate_batman_novice_2',
            sprite: ':battlebate_batman_novice_2'
          }
        } else {
          exampleHero = {
            name: 'fighter',
            label: 'Fighter',
            icon: ':battlebate_fighter_icon',
            sprite: ':battlebate_fighter_novice'
          }
        }
      })

      describe('when the sender is the broadcaster', () => {
        beforeEach(() => {
          message = cb.message({ user: '<broadcaster>', m: '<message>' })
        })

        it('should not decorate the message', () => (
          expect(message.m).toBe('<message>')
        ))
      })

      describe('when avatars are disabled and the player has chosen a hero', () => {
        beforeEach(() => {
          cb.settings.show_avatars = 'no'
          cb.message({ user: '<user>', m: `/hero ${exampleHero.name}` })
          message = cb.message({ user: '<user>', m: '<message>' })
        })

        it('should decorate the message', () => (
          expect(message.m).toBe(`:: ${exampleHero.label} :: <message>`)
        ))
      })

      describe('when avatars are enabled and the player has chosen a hero', () => {
        beforeEach(() => {
          cb.settings.show_avatars = 'yes'
          cb.message({ user: '<user>', m: `/hero ${exampleHero.name}` })
          message = cb.message({ user: '<user>', m: '<message>' })
        })

        it('should decorate the message', () => (
          expect(message.m).toBe(`:: ${exampleHero.sprite} :: <message>`)
        ))
      })
    })

    describe('when a message is received for changing hero', () => {
      describe('when a valid hero is specified', () => {
        let exampleHero
        let changeHero
        let notice

        beforeEach(() => {
          if (theme === 'Batman') {
            exampleHero = {
              name: 'batman',
              label: 'Batman',
              icon: ':battlebate_batman_novce_2',
              sprite: ':battlebate_batman_novice_2'
            }

            notice = 'You are Batman - every 200 tokens tipped wins you: [random prize]! :: Type /hero for more'
          } else {
            exampleHero = {
              name: 'fighter',
              label: 'Fighter',
              icon: ':battlebate_fighter_icon',
              sprite: ':battlebate_fighter_novice'
            }

            notice = 'You are a Fighter - every 200 tokens tipped wins you: [random prize]! :: Type /hero for more'
          }
        })

        beforeEach(() => {
          const entrance = { user: '<user>' }

          cb.enter(entrance)
          changeHero = jest.spyOn(game.players.get('<user>'), 'changeHero')

          cb.message({ user: '<user>', m: `/hero ${exampleHero.name}` })
        })

        it('should change the player\'s hero', () => (
          expect(changeHero).toHaveBeenCalledWith(exampleHero.name)
        ))

        it('should whisper info to the player', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith(notice, game.players.get('<user>').name)
        ))
      })

      describe('when an invalid hero is specified', () => {
        let validOptions

        beforeEach(() => {
          const entrance = { user: '<user>' }

          cb.enter(entrance)
          cb.message({ user: '<user>', m: '/hero <null>' })

          if (theme === 'Batman') {
            validOptions = 'batman, alfred, azrael, nightwing'
          } else {
            validOptions = 'fighter, bard, warlock, ranger'
          }
        })

        it('should show a notice', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith(`Unknown option selected for /hero command. Valid options are: ${validOptions}.`, '<user>')
        ))
      })

      describe('when no hero is specified', () => {
        let showHeroes

        beforeEach(() => {
          const entrance = { user: '<user>' }
          showHeroes = jest.spyOn(game.info, 'showHeroes')

          cb.enter(entrance)
          cb.message({ user: '<user>', m: '/hero' })
        })

        it('should show hero help', () => (
          expect(showHeroes).toHaveBeenCalledWith(game.players.get('<user>'))
        ))
      })
    })

    describe('when a message is received to show the guide to the user', () => {
      let player
      let help

      beforeEach(() => {
        help = jest.spyOn(game.info, 'showHelp')
        cb.message({ user: '<user>', m: '/help' })

        player = game.players.get('<user>')
      })

      it('should show guide', () => {
        expect(help).toHaveBeenCalledWith(player)
      })
    })

    describe('when a message is received to show the guide to everyone', () => {
      let showTLDR

      beforeEach(() => {
        showTLDR = jest.spyOn(game.info, 'showTLDR')
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/tldr all', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not show the guide', () => (
          expect(showTLDR).not.toHaveBeenCalled()
        ))
      })

      describe('when the user is a mod', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/tldr all', is_mod: true })
        })

        it('should show hero info', () => (
          expect(showTLDR).toHaveBeenCalled()
        ))
      })

      describe('when the user is the broadcaster', () => {
        beforeEach(() => {
          cb.message({ user: '<broadcaster>', m: '/tldr all', is_mod: false })
        })

        it('should show hero info', () => (
          expect(showTLDR).toHaveBeenCalled()
        ))
      })
    })

    describe('when a message is received to show TLDR info to a user', () => {
      let showTLDR
      let player

      beforeEach(() => {
        showTLDR = jest.spyOn(game.info, 'showTLDR')
        cb.message({ user: '<user>', m: '/tldr', is_mod: false })

        player = game.players.get('<user>')
      })

      it('should show the TLDR', () => (
        expect(showTLDR).toHaveBeenCalledWith(player)
      ))
    })

    describe('when a message is received to show TLDR info to everyone', () => {
      let showTLDR

      beforeEach(() => {
        showTLDR = jest.spyOn(game.info, 'showTLDR')
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/tldr all', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not show hero info', () => (
          expect(showTLDR).not.toHaveBeenCalled()
        ))
      })

      describe('when the user is a mod', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/tldr all', is_mod: true })
        })

        it('should show hero info', () => (
          expect(showTLDR).toHaveBeenCalled()
        ))
      })

      describe('when the user is the broadcaster', () => {
        beforeEach(() => {
          cb.message({ user: '<broadcaster>', m: '/tldr all', is_mod: false })
        })

        it('should show hero info', () => (
          expect(showTLDR).toHaveBeenCalled()
        ))
      })
    })

    describe('when a message is received to enable/disable player avatars', () => {
      describe('when the user is a mod', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/avatars off', is_mod: true })
        })

        it('should change the setting', () => (
          expect(cb.settings.show_avatars).toBe('no')
        ))
      })

      describe('when the user is the broadcaster', () => {
        beforeEach(() => {
          cb.message({ user: '<broadcaster>', m: '/avatars off', is_mod: false })
        })

        it('should change the setting', () => (
          expect(cb.settings.show_avatars).toBe('no')
        ))
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/avatars off', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not change the setting', () => (
          expect(cb.settings.show_avatars).toBe('yes')
        ))
      })

      describe('when a missing or invalid argument is provided', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/avatars', is_mod: true })
        })

        it('should notify the user', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('You dun goofed. Try /avatars on|off', '<user>')
        ))

        it('should not change the setting', () => (
          expect(cb.settings.show_avatars).toBe('yes')
        ))
      })
    })

    describe('when a message is received to toggle dice rolling', () => {
      beforeEach(() => {
        game.cb.settings.prize_rolling = 'on'
      })

      describe('when the user is a mod', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/dicerolls off', is_mod: true })
        })

        it('should change the setting', () => (
          expect(cb.settings.prize_rolling).toBe('off')
        ))
      })

      describe('when the user is the broadcaster', () => {
        beforeEach(() => {
          cb.message({ user: '<broadcaster>', m: '/dicerolls off', is_mod: false })
        })

        it('should change the setting', () => (
          expect(cb.settings.prize_rolling).toBe('off')
        ))
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/dicerolls off', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not change the setting', () => (
          expect(cb.settings.prize_rolling).not.toBe('off')
        ))
      })

      describe('when a missing or invalid argument is provided', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/dicerolls', is_mod: true })
        })

        it('should notify the user', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('You dun goofed. Try /dicerolls on|off', '<user>')
        ))

        it('should not change the setting', () => (
          expect(cb.settings.prize_rolling).not.toBe('off')
        ))
      })
    })

    describe('when a message is received to show prizes', () => {
      let showPrizes

      beforeEach(() => {
        showPrizes = jest.spyOn(game.info, 'showPrizes')
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/prizes', is_mod: false })
        })

        it('should show the prizes', () => (
          expect(showPrizes).toHaveBeenCalledWith(game.players.get('<user>'))
        ))
      })
    })

    describe('when a message is received to show chat prizes', () => {
      let showPrizes

      beforeEach(() => {
        showPrizes = jest.spyOn(game.info, 'showPrizes')
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/prizes all', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not show the prizes', () => (
          expect(showPrizes).not.toHaveBeenCalled()
        ))
      })

      describe('when the user is a mod', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/prizes all', is_mod: true })
        })

        it('should show the prizes', () => (
          expect(showPrizes).toHaveBeenCalled()
        ))
      })

      describe('when the user is the broadcaster', () => {
        beforeEach(() => {
          cb.message({ user: '<broadcaster>', m: '/prizes all', is_mod: false })
        })

        it('should show the prizes', () => (
          expect(showPrizes).toHaveBeenCalled()
        ))
      })
    })

    describe('when a message is received to show recent prize winners', () => {
      let showWinners

      beforeEach(() => {
        showWinners = jest.spyOn(game.info, 'showWinners')
      })

      describe('when the user is authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/winners', is_mod: true })
          cb.message({ user: '<broadcaster>', m: '/winners', is_mod: false })
        })

        it('should show prize winners', () => (
          expect(showWinners).toHaveBeenCalledTwice
        ))
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/winners', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not show winners', () => (
          expect(showWinners).not.toHaveBeenCalled()
        ))
      })
    })

    describe('when a message is received to set the goal', () => {
      let spawnEnemy

      beforeEach(() => {
        spawnEnemy = jest.spyOn(game, 'spawnEnemy')
      })

      describe('when the token amount is valid', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/goal 1000', is_mod: true })
        })

        it('should spawn a new enemy', () => (
          expect(spawnEnemy).toHaveBeenCalled()
        ))
      })

      describe('when the token amount is invalid', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/goal 0', is_mod: true })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Choose a token amount greater than 0 (e.g. /goal 1000)', '<user>')
        ))

        it('should not spawn an enemy', () => (
          expect(spawnEnemy).not.toHaveBeenCalled()
        ))
      })

      describe('when the user is not a authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/goal 0', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not have spawned an enemy', () => (
          expect(spawnEnemy).not.toHaveBeenCalled()
        ))
      })
    })

    describe('when a returning player enters', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.enter({ user: '<user>' })
      })
    })
  })
}

suite()

export default suite
