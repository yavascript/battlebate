/* eslint-env jest */

'use strict'

import Messenger, { colors } from './messenger'

describe('Messenger', () => {
  const cb = {
    sendNotice: jest.fn()
  }

  const enemy = {
    graphic: { emoji: 'ancient-dragon' }
  }

  let messenger

  beforeEach(() => {
    messenger = new Messenger(cb)
    jest.spyOn(messenger, 'send')
  })

  describe('initialize', () => {
    it('should inejct the `cb` dependency', () => {
      expect(messenger.cb).toBe(cb)
    })
  })

  describe.each([
    { color: '<color>' },
    { user: '<user>', color: '<color>' },
    { group: '<group>', color: '<color>' }
  ])('#send', ({ user, group, color }) => {
    beforeEach(() => {
      messenger.send('<message>', { user, group, color })
    })

    it('should send a message', () => {
      expect(cb.sendNotice).toHaveBeenCalledWith('<message>', user, null, color, 'bold', group)
    })
  })

  describe('#say', () => {
    beforeEach(() => {
      messenger.say('<message>')
    })

    it('should send a message to chat', () => {
      expect(messenger.send).toHaveBeenCalledWith('<message>', { color: colors.get('say') })
    })
  })

  describe('#whisper', () => {
    beforeEach(() => {
      messenger.whisper('<message>', '<user>')
    })

    it('should send a message to the user', () => {
      expect(messenger.send).toHaveBeenCalledWith('<message>', { user: '<user>', color: colors.get('whisper') })
    })
  })

  describe('#reward', () => {
    beforeEach(() => {
      messenger.reward({ name: '<user>' }, '<prize>')
    })

    it('should send a message to chat', () => {
      expect(messenger.send).toHaveBeenCalledWith(':battlebate_die_bonus :: <user> has won: --- <prize>! ---', { color: colors.get('reward') })
    })
  })

  describe('#levelUp', () => {
    beforeEach(() => {
      messenger.levelUp({ name: '<user>', level: 2, hero: { levelUpPhrase: '<phrase>' } }, '<prize>')
    })

    it('should send a message to chat', () => (
      expect(messenger.send).toHaveBeenCalledWith(':battlebate_level_up :: <user> reached Level 2!', { color: colors.get('level-up') })
    ))
  })

  describe('#taunt', () => {
    beforeEach(() => {
      messenger.taunt(enemy, '<message>')
    })

    it('should send a message to chat', () => (
      expect(messenger.send).toHaveBeenCalledWith(':battlebate_ancient_dragon_3 :: <message>', { color: colors.get('taunt') })
    ))
  })
})
