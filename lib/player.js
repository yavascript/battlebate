/**
 * @overview Game party member
 */

'use strict'

import sample from 'lodash/sample'

import { BatmanHeroes, FantasyHeroes } from './themes'

/**
 * Amount needed to level up
 * @private
 * @type {number}
 */
let LEVEL_UP_COST = 200

/**
 * @class Player
 */
class Player {
  /**
   * Sets a static `Player` setting
   * @param {string} setting - Identifier of setting
   * @param {*} value - Value to set
   */
  static configure (setting, value) {
    if (setting === 'LEVEL_UP_COST') {
      LEVEL_UP_COST = value
    }
  }

  /**
   * Sets theme for available heroes
   * @param {string} theme - Name of the theme to use
   */
  static setTheme (theme) {
    switch (theme) {
      case 'fantasy':
        this.heroes = FantasyHeroes
        break
      case 'batman':
        this.heroes = BatmanHeroes
        break
    }
  }

  /**
   * @constructs Player
   * @param {string} name - Username of the player
   * @param {string} singleHero - Restrict to a single hero, instead of random
   */
  constructor (name, singleHero = false) {
    /** @type {string} */
    this.name = name

    /** @type {number}  */
    this.xp = 0

    /** @type {Object} */
    this.hero = Player.heroes.get(
      singleHero
        ? Player.heroes.keys().next().value
        : sample([...Player.heroes.keys()])
    )
  }

  /**
   * Renders an emoji/avatar for the player
   * @readonly
   */
  get emoji () {
    if (!this.hero) {
      return null
    } else if (this.level >= 3) {
      return this.hero.graphics.get('legendary')
    } else if (this.level === 2) {
      return this.hero.graphics.get('elite')
    }

    return this.hero.graphics.get('novice')
  }

  /**
   * Calculates player level based on current XP and the level up requirement
   * @readonly
   */
  get level () {
    return Math.ceil((this.xp + 1) / LEVEL_UP_COST)
  }

  /**
   * Change the character class of the player
   * @param   {string}  selectedClass - Selected character class
   * @returns {Object} `Player` instance
   */
  changeHero (selectedClass) {
    this.hero = Player.heroes.get(selectedClass)

    return this
  }

  /**
   * Attack a given enemy
   * @param   {Enemy}  enemy - The enemy to attack
   * @param   {number} damage - Amount of damage to apply
   * @returns {Object} Attack results
   */
  attack (enemy, damage) {
    const priorLevel = this.level

    // Apply damage
    enemy.damage(damage)

    // Acrue experience points
    this.xp += damage

    return {
      levelsGained: this.level - priorLevel
    }
  }
}

Player.setTheme('fantasy')

export default Player
