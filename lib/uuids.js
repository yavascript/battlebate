/**
 * @overview Lookup for image resource UUIDs across different environments
 */

const UUID_BACKGROUND = Symbol('UUID_BACKGROUND')
const UUID_HEALTH_FILL = Symbol('UUID_HEALTH_FILL')
const UUID_HEALTH_TROUGH = Symbol('UUID_HEALTH_TROUGH')
const UUID_JOKER = Symbol('UUID_JOKER')
const UUID_DEATHSTROKE = Symbol('UUID_DEATHSTROKE')
const UUID_BANE = Symbol('UUID_BANE')
const UUID_LIZARD = Symbol('UUID_LIZARD')
const UUID_OGRE = Symbol('UUID_OGRE')
const UUID_ORC_WARRIOR = Symbol('UUID_ORC_WARRIOR')
const UUID_SKELETON_ARCHER = Symbol('UUID_SKELETON_ARCHER')
const UUID_TROLL = Symbol('UUID_TROLL')
const UUID_DRAUGR = Symbol('UUID_DRAUGR')
const UUID_SKELETON_WARRIOR = Symbol('UUID_SKELETON_WARRIOR')
const UUID_ANCIENT_LICH = Symbol('UUID_ANCIENT_LICH')

const UUIDs = {
  config: {
    mode: 'main',
    env: 'production'
  },

  registry: {
    main: {
      test: new Map()
        .set(UUID_BACKGROUND, '18bdcbcc-1be8-44d8-8f07-3d096e54dbe7')
        .set(UUID_HEALTH_FILL, 'ac204e23-6ec0-4e9c-93ed-c9a64bf9dc85')
        .set(UUID_HEALTH_TROUGH, '6feab7f5-b363-4e1f-9722-3ef77faa58a8')
        .set(UUID_JOKER, '9487fad1-baf1-4904-8ba6-75752bc77f07')
        .set(UUID_DEATHSTROKE, 'd0c874e5-85a5-45de-9e8d-d0548a9fd7f7')
        .set(UUID_BANE, 'ed66e539-609c-4a94-9f11-29b92935bcec')
        .set(UUID_LIZARD, 'b6e062b5-3859-4fcc-8e31-992880c90a9a')
        .set(UUID_OGRE, 'd87eeb8d-2c76-422a-a042-86c7d067fd0d')
        .set(UUID_ORC_WARRIOR, 'c426b27c-8ed4-495d-af37-f9858938116d')
        .set(UUID_SKELETON_ARCHER, 'cb50597b-0cb9-4aa1-a828-466cde09c61f')
        .set(UUID_TROLL, '30372868-dc4d-4689-af68-c2ac0e413668')
        .set(UUID_DRAUGR, '2f6265e9-a8af-4d3e-b8be-e648f1b51c19')
        .set(UUID_SKELETON_WARRIOR, '5ff7a152-2bc7-4837-b8fa-ef28971f9564')
        .set(UUID_ANCIENT_LICH, '92d4587d-adf4-4fa0-bc68-8a6088494c06'),
      alpha: new Map()
        .set(UUID_BACKGROUND, '22c6ad0a-0ab8-4167-9c8f-3496f3ac8860')
        .set(UUID_HEALTH_FILL, 'fc53815c-5dbb-4639-a3d1-582fb516d67a')
        .set(UUID_HEALTH_TROUGH, '54beceaa-4ae9-44f8-baf3-88dab6abe23c'),
      production: new Map()
        .set(UUID_BACKGROUND, '3f669ceb-3a86-491e-b55c-dff98a97e95b')
        .set(UUID_HEALTH_FILL, '0a99bb71-2d59-4f52-8d90-b5bb3a876723')
        .set(UUID_HEALTH_TROUGH, 'ea47fd61-8b21-4c07-b058-011e5121346a')
        .set(UUID_JOKER, '0ee14ae7-f5d9-4e7c-a070-ee2b5c961c04')
        .set(UUID_DEATHSTROKE, '694a7a95-b5d1-4c39-9e76-509fddc25ce3')
        .set(UUID_BANE, '2fd60857-a6ce-4c0d-b99c-66b9f67c89ea')
        .set(UUID_LIZARD, 'e59b117e-54b3-41ea-ad55-918414cb19e9')
        .set(UUID_OGRE, '624e671f-ed84-4608-925a-c8ebe828657e')
        .set(UUID_ORC_WARRIOR, '03b25d74-2318-4028-9b84-e7c6fc9c76c0')
        .set(UUID_SKELETON_ARCHER, 'f751988f-c286-4ee0-8988-56b116dd9038')
        .set(UUID_TROLL, '313a8177-9c25-4f8d-a54d-44a96666a5cf')
        .set(UUID_DRAUGR, '589a08f6-b14f-453c-8add-89fb30159d38')
        .set(UUID_SKELETON_WARRIOR, '1299f264-f0dd-45ce-ad12-ed3cf75f1f6b')
        .set(UUID_ANCIENT_LICH, '5b3b8447-2ef9-4168-84bd-34e60c8d3ae6')
    },
    lite: {
      test: new Map()
        .set(UUID_BACKGROUND, 'd69fe822-980e-4994-8802-0663dc99a161')
        .set(UUID_HEALTH_FILL, 'fc53815c-5dbb-4639-a3d1-582fb516d67a')
        .set(UUID_HEALTH_TROUGH, '54beceaa-4ae9-44f8-baf3-88dab6abe23c')
        .set(UUID_JOKER, '75755324-e258-4ba0-bd40-a74e087b7cdb')
        .set(UUID_DEATHSTROKE, '4149aee4-c243-49c6-86c8-bfdcd9eedfd1')
        .set(UUID_BANE, '...')
        .set(UUID_LIZARD, '60a363a3-ae63-43e9-a5f6-c5cc289fa9fc')
        .set(UUID_OGRE, 'b7bc3c04-e32c-4107-b5b2-7023daac9c12')
        .set(UUID_ORC_WARRIOR, '...')
        .set(UUID_SKELETON_ARCHER, '855b648c-2d97-401f-b515-300da6fe8908')
        .set(UUID_TROLL, '408f6a95-554d-4745-9c78-e3fb0227f768')
        .set(UUID_DRAUGR, 'ed292fb9-e71f-470d-aaea-81635b70aea2')
        .set(UUID_SKELETON_WARRIOR, '...')
        .set(UUID_ANCIENT_LICH, 'cfe8dab1-7774-4127-ac37-e599df543d99'),
      production: new Map()
        .set(UUID_BACKGROUND, 'f9c3d075-0b2f-4c9d-8ce7-3ee50702058a')
        .set(UUID_HEALTH_FILL, 'db9a00e0-0727-469b-9aaa-6d3f72fe699b')
        .set(UUID_HEALTH_TROUGH, 'f6aa7e73-6149-47c5-9c80-9b7bdbf58d96')
        .set(UUID_JOKER, '5984d1c9-f547-451e-881a-e1152c026cdd')
        .set(UUID_DEATHSTROKE, 'dcd3a4cd-2e24-4e33-8449-7c21a531c0c0')
        .set(UUID_BANE, 'ee037436-d661-43c0-979b-4b093f038475')
        .set(UUID_LIZARD, '3aa49115-c00c-441d-a3ab-ac4d2663c589')
        .set(UUID_OGRE, '5810b657-db4a-4374-9d4a-203a1ada848f')
        .set(UUID_ORC_WARRIOR, '03b4f857-6a3d-4108-b505-d86f649d459f')
        .set(UUID_SKELETON_ARCHER, '957ae2eb-cf0d-4071-9ce7-30090f98c35b')
        .set(UUID_TROLL, 'f3c3da2d-6c24-456d-96ae-a047ec2133a7')
        .set(UUID_DRAUGR, '996fa808-940f-4bba-8305-806976bbe3a8')
        .set(UUID_SKELETON_WARRIOR, '951607aa-fba4-405d-b572-d36498d1dd0e')
        .set(UUID_ANCIENT_LICH, '367558d5-36a7-41a5-844a-234601f59e5e')
    }
  },

  get (id) {
    if (global.MODE === 'main') {
      switch (global.ENV) {
        case 'test':
          return this.registry.main.test.get(id)
        case 'alpha':
          return this.registry.main.alpha.get(id)
        case 'production':
        default:
          return this.registry.main.production.get(id)
      }
    } else {
      switch (global.ENV) {
        case 'test':
          return this.registry.lite.test.get(id)
        case 'production':
        default:
          return this.registry.lite.production.get(id)
      }
    }
  }
}

export default UUIDs

export {
  UUID_BACKGROUND,
  UUID_HEALTH_FILL,
  UUID_HEALTH_TROUGH,
  UUID_JOKER,
  UUID_DEATHSTROKE,
  UUID_BANE,
  UUID_LIZARD,
  UUID_OGRE,
  UUID_ORC_WARRIOR,
  UUID_SKELETON_ARCHER,
  UUID_TROLL,
  UUID_DRAUGR,
  UUID_SKELETON_WARRIOR,
  UUID_ANCIENT_LICH
}
