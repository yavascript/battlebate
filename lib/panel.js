/**
 * @overview Manages game/chat panel
 */

import startCase from 'lodash/startCase'

import UUIDs, { UUID_BACKGROUND, UUID_HEALTH_TROUGH, UUID_HEALTH_FILL } from './uuids'

const image = Object.freeze({
  type: 'image'
})

const header = Object.freeze({
  'type': 'text',
  'color': 'orange',
  'font-family': 'Segoe UI, sans-serif',
  'font-size': 11
})

const text = Object.freeze({
  'type': 'text',
  'color': 'white',
  'font-family': 'Segoe UI, sans-serif',
  'font-size': 10
})

class Panel {
  constructor (cb, game) {
    this.cb = cb
    this.game = game

    this.cb.onDrawPanel(this.onDrawPanel.bind(this))
  }

  get healthOffset () {
    const min = -127
    const steps = 188

    return min + (this.game.enemy.hp / this.game.enemy.maxHP) * steps
  }

  onDrawPanel () {
    const { id: avatarGraphic, x: avatarX, y: avatarY } = this.game.enemy.graphic
    const headerText = startCase(this.game.enemy.name)
    const hpText = `${this.game.enemy.hp} tokens left`

    let hintX

    if (global.MODE === 'main') {
      hintX = this.cb.settings.prize_rolling === 'on'
        ? 155
        : 184
    } else {
      hintX = 167
    }

    let footerText = this.game.totalKills
      ? `${this.game.totalKills} villains defeated`
      : `No villains defeated yet`

    if (this.cb.settings.show_total_tokens === 'yes') {
      footerText += ` (${this.game.totalDamage} tokens)`
    }

    const layers = [
      { ...image, fileID: UUIDs.get(UUID_HEALTH_TROUGH), left: 61, top: 27 },
      { ...image, fileID: UUIDs.get(UUID_HEALTH_FILL), left: this.healthOffset, top: 27 },
      { ...image, fileID: UUIDs.get(UUID_BACKGROUND) },
      { ...image, fileID: UUIDs.get(avatarGraphic), left: avatarX, top: avatarY },
      { ...header, text: headerText, left: 62, top: 9 },
      { ...text, text: hpText, left: 115, top: 27 },
      { ...text, text: footerText, left: 62, top: 45 }
    ]

    let hintText

    if (global.MODE === 'main') {
      hintText = this.cb.settings.prize_rolling === 'on'
        ? `Dice Rolls: ${this.cb.settings.attack_cost} tokens`
        : 'Dice Rolls: Off'
    } else {
      hintText = 'Tips win the battle!'
    }

    layers.push({ ...text, text: hintText, left: hintX, top: 10 })

    return { template: 'image_template', layers }
  }

  redraw () {
    this.cb.drawPanel()

    return this
  }
}

export default Panel
