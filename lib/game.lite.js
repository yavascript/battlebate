/**
 * @overview Main game module
 */

'use strict'

import { version as VERSION } from '../package.json'

import { BatmanAnnouncements, FantasyAnnouncements } from './themes'

import Command from './command'
import Enemy from './enemy'
import Graphics from './graphics'
import Info from './info'
import Messenger from './messenger'
import Panel from './panel'
import Player from './player'

function decorateMessage (message) {
  if (message.user === this.cb.room_slug) {
    return message
  }

  if (this.cb.settings.show_avatars === 'yes') {
    const player = this.fetchPlayer(message.user)
    const label = Graphics.render(player.emoji)

    message.m = `:: ${label} :: ${message.m}`
  }

  return message
}

/**
 * Execute an attack against an enemy by a player
 * @param   {Object} player - Attacking player
 * @param   {number} damage - Damage to apply
 * @returns {undefined}
 */
function attack (player, damage) {
  // Apply damage to enemy
  player.attack(this.enemy, damage)
}

/**
 * Handle users entering
 * @param   {string} entrance - Chat entrance info
 * @returns {undefined}
 */
function handleEnter (entrance) {
  let player

  if (entrance.user === this.cb.room_slug) {
    return
  }

  if (!this.players.has(entrance.user)) {
    player = this.addPlayer(entrance.user)
  } else {
    player = this.players.get(entrance.user)
  }

  this.info.showTLDR(player)
}

/**
 * Handle chat messages
 * @param {Object} message - Chat message
 * @returns {Object} Chat message
 */
function handleMessage (message) {
  const cmdExecution = this.command.evaluate(message)

  if (cmdExecution !== null) {
    return Object.assign(message, { 'X-Spam': true })
  }

  return decorateMessage.call(this, message)
}

/**
 * Handle chat tips
 * @param {Object} tip - Hashmap of tip properties
 * @returns {undefined}
 */
function handleTip (tip) {
  const user = tip.from_user
  const player = this.fetchPlayer(user)
  const previousHP = this.enemy.hp

  // Execute the attack
  attack.call(this, player, tip.amount)

  // Track cummulative damage
  this.totalDamage += tip.amount

  // Player drops enemy
  if (this.enemy.hp === 0) {
    // Determine number of kills (goals reached) and any leftover damage to apply
    const kills = 1 + Math.floor((tip.amount - previousHP) / this.enemy.maxHP)
    const leftoverDamage = (tip.amount - previousHP) % this.enemy.maxHP

    // Announce the kill
    this.messenger.say(this.announcements.defeat(player, this.enemy))

    this.spawnEnemy()
    this.enemy.damage(leftoverDamage)

    this.totalKills += kills
  }

  this.panel.redraw()
}

/**
 * @class Game
 */
class Game {
  /**
   * @constructs Game
   * @param {Object} cb - Chaturbate interface
   */
  constructor (cb) {
    /** @type {Object} */
    this.cb = cb

    /** @type {Enemy|null} */
    this.enemy = null

    /** @type {Map} */
    this.players = new Map()

    /** @type {Command} */
    this.command = new Command({ exactMatching: this.cb.settings.exact_command_matching === 'yes' })

    /** @type {Messenger} */
    this.messenger = new Messenger(cb)

    /** @type {Info} */
    this.info = new Info(this.messenger, this)

    /** @type {number} */
    this.totalDamage = 0

    /** @type {number} */
    this.totalKills = 0

    /** @type {Panel} */
    this.panel = new Panel(cb, this)
  }

  showInfo () {
    this.messenger.whisper(`Running Battlebate Lite v${VERSION}`, this.cb.room_slug)

    return this
  }

  registerCommands () {
    this.command.register('/env', [{ name: 'environment', type: 'directive', values: ['production', 'alpha', 'test'] }], (message, command, args) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      switch (args.environment) {
        case 'test':
          global.ENV = 'test'
          break
        case 'alpha':
          global.ENV = 'alpha'
          break
        case 'production':
        default:
          global.ENV = 'production'
          break
      }

      this.messenger.whisper(`Changing environment to: ${args.environment}`, message.user)
      this.panel.redraw()
    })

    this.command.register('/avatars', [{ name: 'enabled', type: 'directive', values: ['on', 'off', 'text'] }], (message, command, args) => {
      if (!message.is_mod && message.user !== this.cb.room_slug) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      if (args.enabled === null) {
        this.messenger.whisper('You dun goofed. Try /avatars on|off', message.user)
        return
      }

      this.cb.settings.show_avatars = args.enabled === 'on' ? 'yes' : 'no'
      this.messenger.say(`Player avatars have been ${args.enabled === 'on' ? 'enabled' : 'disabled'}.`)
    })

    this.command.register('/goal', [{ name: 'amount', type: 'number', values: [] }], (message, command, args) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      const amount = Number(args.amount)

      if (amount && amount > 0) {
        this.cb.settings.goal_amount = amount

        this.spawnEnemy()
        this.messenger.whisper(`Goal has been updated`, message.user)
      } else {
        this.messenger.whisper('Choose a token amount greater than 0 (e.g. /goal 1000)', message.user)
      }
    })
  }

  /**
   * Replaces current enemy with a newly generated one
   * @returns {Game} (self)
   */
  spawnEnemy () {
    this.enemy = new Enemy({ hp: this.cb.settings.goal_amount })

    const message = this.announcements.spawn(this.enemy)

    this.panel.redraw()
    this.messenger.say(message)

    return this
  }

  setTheme (theme) {
    switch (theme) {
      case 'Batman':
        this.announcements = BatmanAnnouncements
        Player.setTheme('batman')
        Enemy.setTheme('batman')

        break
      case 'Fantasy':
      default:
        this.announcements = FantasyAnnouncements
        Player.setTheme('fantasy')
        Enemy.setTheme('fantasy')

        break
    }

    this.registerCommands()

    return this
  }

  /**
   * Gets a player by username, adding to the game if they're not already playing
   * @param {string} user - Username of the given user
   * @returns {Player} The given player
   */
  fetchPlayer (user) {
    return this.players.has(user)
      ? this.players.get(user)
      : this.addPlayer(user)
  }

  /**
   * Add a given player to the game, by name
   * @param   {string} user - The username of the player to add
   * @returns {Player} `Player` instance
   */
  addPlayer (user) {
    const player = new Player(user, true)

    this.players.set(user, player)

    return player
  }

  taunt () {
    this.messenger.taunt(this.enemy)
    this.messenger.say(this.announcements.hint())

    // Taunt again once every 5 minutes
    this.cb.setTimeout(() => {
      this.taunt()
    }, 300000)

    return this
  }

  /**
   * Start the game, binding event handlers
   * @returns {Game} `Game` instance
   */
  start () {
    // Setup options dropdowns
    this.cb.settings_choices = [
      { name: 'theme', label: 'Theme', type: 'choice', choice1: 'Fantasy', choice2: 'Batman', defaultChoice: 'Fantasy' },
      { name: 'goal_amount', label: 'Goal (auto-repeat)', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 500 },
      { name: 'show_total_tokens', label: 'Show Total Tips', type: 'choice', choice1: 'yes', choice2: 'no', defaultChoice: 'yes' },
      { name: 'show_avatars', label: 'Show Player Avatars', type: 'choice', choice1: 'yes', choice2: 'no', defaultChoice: 'yes' }
    ]

    // Force various settings
    this.cb.settings.exact_command_matching = 'yes'

    // Set the theme
    this.setTheme(this.cb.settings.theme)

    // Display game info
    this.showInfo()

    // Setup enemy to fight
    this.spawnEnemy()

    // Kick off taunts
    this.taunt()

    // Setup chat commands
    this.registerCommands()

    // Attach event handlers
    this.cb.onTip(handleTip.bind(this))
    this.cb.onMessage(handleMessage.bind(this))
    this.cb.onEnter(handleEnter.bind(this))

    return this
  }
}

export default Game
