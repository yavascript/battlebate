/**
 * @overview Main game module
 */

'use strict'

import sample from 'lodash/sample'
import startCase from 'lodash/startCase'
import times from 'lodash/times'
import { version as VERSION } from '../package.json'

import { BatmanAnnouncements, FantasyAnnouncements } from './themes'

import Command from './command'
import Enemy from './enemy'
import Graphics from './graphics'
import Info from './info'
import Messenger from './messenger'
import Panel from './panel'
import Player from './player'

function decorateMessage (message) {
  if (message.user === this.cb.room_slug) {
    return message
  }

  const player = this.fetchPlayer(message.user)
  let label

  if (this.cb.settings.show_avatars === 'yes') {
    label = Graphics.render(player.emoji)
  } else {
    label = startCase(player.hero.name)
  }

  message.m = `:: ${label} :: ${message.m}`

  return message
}

/**
 * Execute an attack against an enemy by a player
 * @param   {Object} player - Attacking player
 * @param   {number} damage - Damage to apply
 * @returns {undefined}
 */
function attack (player, damage) {
  let attacks = Math.floor(damage / this.cb.settings.attack_cost)
  let levelUpPrize

  // If multi-roll is disabled, take the single highest roll
  if (attacks > 1 && this.cb.settings.multi_roll === 'no') {
    attacks = 1
  }

  // Apply damage to enemy & keep track of levels gained
  const { levelsGained } = player.attack(this.enemy, damage)

  // Hand out common prize(s)
  if (this.cb.settings.prize_rolling === 'on') {
    times(attacks, roll => {
      const prize = sample(this.prizes)

      this.logWinning(player, prize)
      this.messenger.reward(player, prize, this.prizes.indexOf(prize) + 1)
    })
  }

  // Hand out Level Up prize(s)
  times(levelsGained, level => {
    levelUpPrize = this.cb.settings[player.hero.bonusPrize]
      ? this.cb.settings[player.hero.bonusPrize]
      : sample(this.prizes)

    this.logWinning(player, levelUpPrize)
  })

  if (levelsGained >= 1) {
    times(levelsGained, () => {
      this.messenger.reward(player, levelUpPrize)
    })

    this.messenger.levelUp(player)
  }
}

/**
 * Handle users entering
 * @param   {string} entrance - Chat entrance info
 * @returns {undefined}
 */
function handleEnter (entrance) {
  let player

  if (entrance.user === this.cb.room_slug) {
    return
  }

  if (!this.players.has(entrance.user)) {
    player = this.addPlayer(entrance.user)
    this.messenger.whisper(this.announcements.heroChange(player, this.cb), player.name)
  } else {
    player = this.players.get(entrance.user)
  }

  this.info.showTLDR(player)
}

/**
 * Handle chat messages
 * @param {Object} message - Chat message
 * @returns {Object} Chat message
 */
function handleMessage (message) {
  const cmdExecution = this.command.evaluate(message)

  if (cmdExecution !== null) {
    return Object.assign(message, { 'X-Spam': true })
  }

  return decorateMessage.call(this, message)
}

/**
 * Handle chat tips
 * @param {Object} tip - Hashmap of tip properties
 * @returns {undefined}
 */
function handleTip (tip) {
  const user = tip.from_user
  const player = this.fetchPlayer(user)
  const previousHP = this.enemy.hp

  // Execute the attack
  attack.call(this, player, tip.amount)

  // Track cummulative damage
  this.totalDamage += tip.amount

  // Player drops enemy
  if (this.enemy.hp === 0) {
    // Determine number of kills (goals reached) and any leftover damage to apply
    const kills = 1 + Math.floor((tip.amount - previousHP) / this.enemy.maxHP)
    const leftoverDamage = (tip.amount - previousHP) % this.enemy.maxHP

    // Announce the kill
    this.messenger.say(this.announcements.defeat(player, this.enemy))

    this.spawnEnemy()
    this.enemy.damage(leftoverDamage)

    this.totalKills += kills
  }

  // Show XP bar
  this.info.showXP(player)

  this.panel.redraw()
}

/**
 * @class Game
 */
class Game {
  /**
   * @constructs Game
   * @param {Object} cb - Chaturbate interface
   */
  constructor (cb) {
    /** @type {Object} */
    this.cb = cb

    /** @type {Enemy|null} */
    this.enemy = null

    /** @type {Map} */
    this.players = new Map()

    /** @type {Array} */
    this.prizes = []

    /** @type {Array.<Object>} */
    this.winners = []

    /** @type {Command} */
    this.command = new Command({ exactMatching: this.cb.settings.exact_command_matching === 'yes' })

    /** @type {Messenger} */
    this.messenger = new Messenger(cb)

    /** @type {Info} */
    this.info = new Info(this.messenger, this)

    /** @type {number} */
    this.totalDamage = 0

    /** @type {number} */
    this.totalKills = 0

    /** @type {Array} */
    this.prizesWon = []

    /** @type {Panel} */
    this.panel = new Panel(cb, this)
  }

  showInfo () {
    const usefulCommands = 'Type /help to see some useful commands!'

    this.messenger.whisper(`Running Battlebate v${VERSION}`, this.cb.room_slug)
    this.messenger.send(usefulCommands, { group: 'red' })
    this.messenger.whisper(usefulCommands, this.cb.room_slug)

    return this
  }

  registerCommands () {
    this.command.register('/env', [{ name: 'environment', type: 'directive', values: ['production', 'alpha', 'test'] }], (message, command, args) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      switch (args.environment) {
        case 'test':
          global.ENV = 'test'
          break
        case 'alpha':
          global.ENV = 'alpha'
          break
        case 'production':
        default:
          global.ENV = 'production'
          break
      }

      this.messenger.whisper(`Changing environment to: ${args.environment}`, message.user)
      this.panel.redraw()
    })

    this.command.register('/hero', [{ name: 'hero', type: 'directive', values: [...Player.heroes.keys()] }], (message, command, args) => {
      const hero = args.hero
      const player = this.fetchPlayer(message.user)

      if (hero) {
        player.changeHero(hero)
        this.messenger.whisper(this.announcements.heroChange(player, this.cb), player.name)
      } else if (message.m.trim() === '/hero') {
        this.info.showHeroes(player)
      } else {
        this.messenger.whisper(`Unknown option selected for /hero command. Valid options are: ${[...Player.heroes.keys()].join(', ')}.`, message.user)
      }
    })

    this.command.register('/tldr', [{ name: 'audience', type: 'directive', values: ['all'] }], (message, command, args) => {
      const audience = args.audience
      const player = this.fetchPlayer(message.user)

      if (audience !== 'all') {
        this.info.showTLDR(player)
        return
      }

      if (message.is_mod || message.user === this.cb.room_slug) {
        this.info.showTLDR()
      } else {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
      }
    })

    this.command.register('/help', null, (message) => {
      const player = this.fetchPlayer(message.user)
      this.info.showHelp(player)
    })

    this.command.register('/avatars', [{ name: 'enabled', type: 'directive', values: ['on', 'off', 'text'] }], (message, command, args) => {
      if (!message.is_mod && message.user !== this.cb.room_slug) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      if (args.enabled === null) {
        this.messenger.whisper('You dun goofed. Try /avatars on|off', message.user)
        return
      }

      this.cb.settings.show_avatars = args.enabled === 'on' ? 'yes' : 'no'
      this.messenger.say(`Player avatars have been ${args.enabled === 'on' ? 'enabled' : 'disabled'}.`)
    })

    this.command.register('/goal', [{ name: 'amount', type: 'number', values: [] }], (message, command, args) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      const amount = Number(args.amount)

      if (amount && amount > 0) {
        this.cb.settings.goal_amount = amount

        this.spawnEnemy()
        this.messenger.whisper(`Goal has been updated`, message.user)
      } else {
        this.messenger.whisper('Choose a token amount greater than 0 (e.g. /goal 1000)', message.user)
      }
    })

    this.command.register('/prizes', [{ name: 'audience', type: 'directive', values: ['all'] }], (message, command, args) => {
      const audience = args.audience
      const player = this.fetchPlayer(message.user)

      if (audience !== 'all') {
        this.info.showPrizes(player)
        return
      }

      if (message.is_mod || message.user === this.cb.room_slug) {
        this.info.showPrizes()
      } else {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
      }
    })

    this.command.register('/dicerolls', [{ name: 'value', type: 'directive', values: ['on', 'off'] }], (message, command, args) => {
      if (!message.is_mod && message.user !== this.cb.room_slug) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      if (args.value === null) {
        this.messenger.whisper('You dun goofed. Try /dicerolls on|off', message.user)
        return
      }

      this.cb.settings.prize_rolling = args.value
      this.panel.redraw()
      this.messenger.say(`Dice rolling has been ${args.value === 'on' ? 'enabled' : 'disabled'}.`)
    })

    this.command.register('/winners', null, (message, command) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user)
        return
      }

      this.info.showWinners()
    })
  }

  setReminder (reminder = null) {
    if (reminder === 'prizes') {
      // Every 10 minutes, show prize info
      this.cb.setTimeout(() => {
        this.info.showPrizes()
        this.setReminder('prizes')
      }, 600000)
    } else if (reminder === 'winners') {
      // Every 4 minutes, show prizes won
      this.cb.setTimeout(() => {
        this.info.showWinners()
        this.setReminder('winners')
      }, 240000)
    }

    return this
  }

  /**
   * Replaces current enemy with a newly generated one
   * @returns {Game} (self)
   */
  spawnEnemy () {
    this.enemy = new Enemy({ hp: this.cb.settings.goal_amount })

    const message = this.announcements.spawn(this.enemy)

    this.panel.redraw()
    this.messenger.say(message)

    return this
  }

  setTheme (theme) {
    switch (theme) {
      case 'Batman':
        this.announcements = BatmanAnnouncements
        Player.setTheme('batman')
        Enemy.setTheme('batman')

        break
      case 'Fantasy':
      default:
        this.announcements = FantasyAnnouncements
        Player.setTheme('fantasy')
        Enemy.setTheme('fantasy')

        break
    }

    this.registerCommands()

    return this
  }

  /**
   * Setup game prizes
   * @returns {Game} `Game` instance
   */
  setupPrizes () {
    const settings = this.cb.settings
    const settingsProperties = Object.keys(settings)

    this.prizes = settingsProperties
      .filter(s => /prize_\d/.test(s) && settings[s].trim().length)
      .map(s => this.cb.settings[s])

    return this
  }

  /**
   * Logs winnings
   * @param {Object} player - Player that won the prize
   * @param {string} prize - Description of the prize won
   * @returns {Game} (self)
   */
  logWinning (player, prize) {
    // Log it
    this.winners.push({
      player,
      prize,
      time: new Date()
    })

    return this
  }

  /**
   * Gets a player by username, adding to the game if they're not already playing
   * @param {string} user - Username of the given user
   * @returns {Player} The given player
   */
  fetchPlayer (user) {
    return this.players.has(user)
      ? this.players.get(user)
      : this.addPlayer(user)
  }

  /**
   * Add a given player to the game, by name
   * @param   {string} user - The username of the player to add
   * @returns {Player} `Player` instance
   */
  addPlayer (user) {
    const player = new Player(user)

    this.players.set(user, player)

    return player
  }

  taunt (showHint = false) {
    this.messenger.taunt(this.enemy)

    if (showHint) {
      this.messenger.say(this.announcements.hint())
    }

    // Taunt again once every 5 minutes
    this.cb.setTimeout(() => {
      this.taunt(true)
    }, 300000)

    return this
  }

  /**
   * Start the game, binding event handlers
   * @returns {Game} `Game` instance
   */
  start () {
    // Setup options dropdowns
    this.cb.settings_choices = [
      { name: 'theme', label: 'Theme', type: 'choice', choice1: 'Fantasy', choice2: 'Batman', defaultChoice: 'Fantasy' },
      { name: 'goal_amount', label: 'Goal (auto-repeat)', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 500 },
      { name: 'prize_rolling', label: 'Dice Rolls', type: 'choice', choice1: 'on', choice2: 'off', defaultValue: 'on' },
      { name: 'attack_cost', label: 'Dice Roll Cost', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 44 },
      { name: 'prize_1', label: 'Dice Roll Prize (1)', type: 'str', defaultValue: 'booty flash' },
      { name: 'prize_2', label: 'Dice Roll Prize (2)', type: 'str', defaultValue: 'boobs flash' },
      { name: 'prize_3', label: 'Dice Roll Prize (3)', type: 'str', defaultValue: '10 hand spanks' },
      { name: 'prize_4', label: 'Dice Roll Prize (4)', type: 'str', required: false },
      { name: 'prize_5', label: 'Dice Roll Prize (5)', type: 'str', required: false },
      { name: 'prize_6', label: 'Dice Roll Prize (6)', type: 'str', required: false },
      { name: 'level_up_amount', label: 'Tips Needed To Level Up', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 200 },
      { name: 'bonus_domination_prize', label: 'Level Up Prize :: Warlock/Azrael', type: 'str', defaultValue: 'nipple clamps' },
      { name: 'bonus_flash_prize', label: 'Level Up Prize :: Ranger/Nightwing', type: 'str', defaultValue: 'any flash' },
      { name: 'bonus_charm_prize', label: 'Level Up Prize :: Bard/Alfred', type: 'str', defaultValue: 'sexy dancing' },
      { name: 'multi_roll', label: 'Advanced :: Multi Dice Rolls When Overtipping', type: 'choice', choice1: 'yes', choice2: 'no', defaultValue: 'no' },
      { name: 'show_total_tokens', label: 'Advanced :: Show Total Tips', type: 'choice', choice1: 'yes', choice2: 'no', defaultChoice: 'yes' },
      { name: 'show_avatars', label: 'Show Player Avatars', type: 'choice', choice1: 'yes', choice2: 'no', defaultChoice: 'yes' }
    ]

    // Force various settings
    this.cb.settings.exact_command_matching = 'yes'

    // Set Level Up requirement
    Player.configure('LEVEL_UP_COST', this.cb.settings.level_up_amount)

    // Set the theme
    this.setTheme(this.cb.settings.theme)

    // Setup prizes (loot, etc.)
    this.setupPrizes()

    // Display game info
    this.showInfo()

    // Show welcome message
    this.info.showTLDR()

    // Setup enemy to fight
    this.spawnEnemy()

    // Kick off taunts
    this.taunt(false)

    // Setup chat commands
    this.registerCommands()

    // Setup reminders
    this.setReminder('prizes')
    this.setReminder('winners')

    // Attach event handlers
    this.cb.onTip(handleTip.bind(this))
    this.cb.onEnter(handleEnter.bind(this))
    this.cb.onMessage(handleMessage.bind(this))

    return this
  }
}

export default Game
