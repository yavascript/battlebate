/* eslint-env jest */

'use strict'

import Command from './command'

function build (options) {
  return new Command(options)
}

describe('Command', () => {
  let command
  let callback
  let message
  let cmd

  beforeEach(() => {
    command = build()
    callback = jest.fn()
  })

  describe('initialize', () => {
    it('should setup an empty `Map` registry of commands', () => (
      expect(command.registry).toBeInstanceOf(Map)
    ))
  })

  describe('#register', () => {
    beforeEach(() => {
      command.register('/test', null, callback)
    })

    it('should add the specified command to the registry', () => (
      expect(command.registry.get('/test')).toEqual({
        pattern: '/test',
        params: null,
        callback
      })
    ))
  })

  describe('#evaluate', () => {
    describe('when exact matching is disabled', () => {
      beforeEach(() => {
        command = build({ exactMatching: false })
        command.register('/test', null, callback)

        message = { m: 'text /test' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => {
        expect(callback).toHaveBeenCalledWith(message, cmd, null)
      })
    })

    describe('when no parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', null, callback)

        message = { m: '/test' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, null)
      ))
    })

    describe('when a [directive] parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'directive', values: ['value'] }], callback)

        message = { m: '/test value' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument: 'value' })
      ))
    })

    describe('when a [any] argument is passed using different casing', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'any', values: ['Value value Value'] }], callback)

        message = { m: '/test value value value' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument: 'value value value' })
      ))
    })

    describe('when a [string] parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'string', values: [] }], callback)

        message = { m: '/test "<value>"' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument: '<value>' })
      ))
    })

    describe('when an [assignment] parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'assignment', values: [] }], callback)

        message = { m: '/test argument=value' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument: 'value' })
      ))
    })

    describe('when multiple parameters are defined', () => {
      beforeEach(() => {
        command.register('/test', [
          { name: 'argument1', type: 'string', values: ['value1'] },
          { name: 'argument2', type: 'assignment', values: ['value2'] }
        ], callback)

        message = { m: '/test "value1" argument=value2' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument1: 'value1', argument2: 'value2' })
      ))
    })

    describe('when a command is executed with special characters', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'any', values: [] }], callback)

        message = { m: '/test +1 /foo <value>' }
        cmd = command.registry.get('/test')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument: '+1 /foo <value>' })
      ))
    })

    describe('when a command that partially matches another is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'any', values: [] }], callback)
        command.register('/tests', [{ name: 'argument', type: 'any', values: [] }], callback)

        message = { m: '/tests <value>' }
        cmd = command.registry.get('/tests')

        command.evaluate(message)
      })

      it('should execute the callback', () => (
        expect(callback).toHaveBeenCalledWith(message, cmd, { argument: '<value>' })
      ))
    })
  })
})
