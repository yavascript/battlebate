/**
 * @ts-check
 * @overview Library of game graphics references (simple emoji codes) and text fallbacks
 */

'use strict'

import assert from 'assert'

const ERROR_UKNOWN_GRAPHIC = 'Unknown graphic specified for `Graphics#render`'

/**
 * All known graphics for the game
 * @type {Map.<string, Object>}
 */
const Registry = new Map()
  .set('die-1', ':battlebate_die1')
  .set('die-2', ':battlebate_die2')
  .set('die-3', ':battlebate_die3')
  .set('die-4', ':battlebate_die4')
  .set('die-5', ':battlebate_die5')
  .set('die-6', ':battlebate_die6')
  .set('die-bonus', ':battlebate_die_bonus')
  .set('level-up', ':battlebate_level_up')

  // Fantasy graphics
  .set('ancient-dragon', ':battlebate_ancient_dragon_3')
  .set('awakened-tree', ':battlebate_awakened_tree')
  .set('death-knight', ':battlebate_death_knight_2')
  .set('draugr', ':battlebate_draugr')
  .set('fire-elemental', ':battlebate_fire_elemental')
  .set('ghost', ':battlebate_ghost')
  .set('half-dragon', ':battlebate_half_dragon')
  .set('lich', ':battlebate_lich_2')
  .set('lizardman', ':battlebate_lizardman')
  .set('merfolk-warrior', ':battlebate_merfolk_warrior_2')
  .set('myrmidon', ':battlebate_myrmidon')
  .set('ogre', ':battlebate_ogre')
  .set('orc-chieftain', ':battlebate_orc_chieftain')
  .set('orc-hellbringer', ':battlebate_orc_hellbringer_2')
  .set('skeleton-archer', ':battlebate_skeleton_archer')
  .set('skeleton-swordsman', ':battlebate_skeleton_swordsman')
  .set('troll', ':battlebate_troll')
  .set('undead-dragon', ':battlebate_undead_dragon_3')
  .set('wraith', ':battlebate_wraith')
  .set('bard-novice', ':battlebate_bard_novice')
  .set('bard-elite', ':battlebate_bard_elite')
  .set('bard-legendary', ':battlebate_bard_legendary')
  .set('fighter-novice', ':battlebate_fighter_novice')
  .set('fighter-elite', ':battlebate_fighter_elite')
  .set('fighter-legendary', ':battlebate_fighter_legendary')
  .set('ranger-novice', ':battlebate_ranger_novice')
  .set('ranger-elite', ':battlebate_ranger_elite')
  .set('ranger-legendary', ':battlebate_ranger_legendary')
  .set('warlock-novice', ':battlebate_warlock_novice')
  .set('warlock-elite', ':battlebate_warlock_elite')
  .set('warlock-legendary', ':battlebate_warlock_legendary')

  // Batman graphics
  .set('batman-novice', ':battlebate_batman_novice_2')
  .set('batman-elite', ':battlebate_batman_elite_3')
  .set('batman-legendary', ':battlebate_batman_legendary_2')
  .set('alfred-novice', ':battlebate_alfred_novice_2')
  .set('alfred-elite', ':battlebate_alfred_elite_2')
  .set('alfred-legendary', ':battlebate_alfred_legendary_2')
  .set('nightwing-novice', ':battlebate_nightwing_novice_2')
  .set('nightwing-elite', ':battlebate_nightwing_elite_2')
  .set('nightwing-legendary', ':battlebate_nightwing_legendary_2')
  .set('azrael-novice', ':battlebate_azrael_novice_2')
  .set('azrael-elite', ':battlebate_azrael_elite_2')
  .set('azrael-legendary', ':battlebate_azrael_legendary_2')
  .set('joker', ':battlebate_joker_3')
  .set('bane', ':battlebate_bane_3')
  .set('deathstroke', ':battlebate_deathstroke_3')

class Graphics {
  /**
   * Renders a given graphic
   * @param   {Object} name - Name of the graphic to "render"
   * @returns {string}
   */
  static render (name) {
    assert(Registry.has(name), `${ERROR_UKNOWN_GRAPHIC}: "${name}"`)

    return Registry.get(name)
  }
}

export {
  Registry,
  ERROR_UKNOWN_GRAPHIC
}

export default Graphics
