'use strict'

import Game from './game'

global.ENV = 'production'
global.MODE = 'main'

const game = new Game(global.cb)
game.start()
