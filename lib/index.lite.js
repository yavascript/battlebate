'use strict'

import GameLite from './game.lite'

global.ENV = 'production'
global.MODE = 'lite'

const game = new GameLite(global.cb)
game.start()
