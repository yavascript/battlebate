/* eslint-env jest */

'use strict'

import Enemy from './enemy'
import Player from './player'

describe('Player', () => {
  Player.setTheme('fantasy')

  describe('initialize', () => {
    const player = new Player('<name>')

    it('should return an instance of `Player`', () => {
      expect(player).toBeInstanceOf(Player)
    })

    it('should assign initial property values', () => {
      expect(player.name).toBe('<name>')
      expect(player.level).toBe(1)
      expect(player.xp).toBe(0)
    })
  })

  describe('.level', () => {
    const player = new Player('<name>')

    it('should reflect current xp', () => {
      player.xp = 200
      expect(player.level).toBe(2)
    })
  })

  describe('.emoji', () => {
    it('should render a graphic to represent the player based on class and rank', () => {
      const heroes = ['bard', 'fighter', 'warlock']

      heroes.forEach(heroClass => {
        const player = new Player('<name>')

        player.changeHero(heroClass)
        expect(player.emoji).toBe(`${heroClass}-novice`)

        player.xp += 200
        expect(player.emoji).toBe(`${heroClass}-elite`)

        player.xp += 200
        expect(player.emoji).toBe(`${heroClass}-legendary`)
      })
    })
  })

  describe('#changeHero', () => {
    const player = new Player('<name>')
    player.changeHero('bard')

    it('should assign the given character class', () => {
      expect(player.hero.name).toBe('bard')
    })
  })

  describe.each([
    { dmg: 10, levelsGained: 0 },
    { dmg: 199, levelsGained: 0 },
    { dmg: 200, levelsGained: 1 },
    { dmg: 201, levelsGained: 1 }
  ])('#attack', ({ dmg, levelsGained }) => {
    const player = new Player('<name>')
    const enemy = new Enemy({ hp: 1000 })
    let results

    it('should deal damage to the target', () => {
      const damage = jest.spyOn(enemy, 'damage')

      results = player.attack(enemy, dmg)
      expect(damage).toHaveBeenCalledWith(dmg)
    })

    it('should acrue experience points', () => {
      expect(player.xp).toBe(dmg)
    })

    it('should return a result object with `levelsGained`', () => {
      expect(results).toMatchObject({ levelsGained })
    })
  })
})
