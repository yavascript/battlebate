/* eslint-env jest */

'use strict'

import Graphics, { Registry, ERROR_UKNOWN_GRAPHIC } from './graphics'

describe('Graphics', () => {
  describe('#render', () => {
    it('should throw an error if the specified graphic is unknown', () => (
      expect(Graphics.render.bind(null, '<unknown>')).toThrow(ERROR_UKNOWN_GRAPHIC)
    ))

    it('should return each graphic correctly', () => (
      Registry.forEach((value, name) => {
        const rendering = Graphics.render(name)
        expect(rendering).toBe(value)
      })
    ))
  })
})
