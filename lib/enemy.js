/**
 * @ts-check
 * @overview The enemy player characters face off against, i.e. the main goal
 */

'use strict'

import shuffle from 'lodash/shuffle'

import { BatmanEnemies, FantasyEnemies } from './themes'

/**
 * @class Enemy
 */
class Enemy {
  /**
   * Sets theme for available heroes
   * @param {string} theme - Name of the theme to use
   */
  static setTheme (theme) {
    let set

    switch (theme) {
      case 'fantasy':
      default:
        set = new Set(shuffle(FantasyEnemies))
        break
      case 'batman':
        set = new Set(shuffle(BatmanEnemies))
        break
    }

    this.enemies = {
      iterator: set.values(),
      set: set
    }
  }

  /**
   * Generate a new enemy
   * @returns {Enemy} Chosen enemy persona
   */
  static generate () {
    let personas = this.enemies
    let persona = personas.iterator.next()

    // Reset the iterator, if needed
    if (persona.done) {
      personas.iterator = personas.set.values()
      persona = personas.iterator.next()
    }

    return persona.value
  }

  /**
   * @constructs Enemy
   * @param {Object}  attributes - Initial enemy properties
   * @param {string}  attributes.name - Name of the enemy
   * @param {string}  attributes.emoji - Icon/visual
   * @param {number}  attributes.hp - Total hitpoints
   */
  constructor (attributes) {
    const persona = Enemy.generate(attributes.hp)

    /** @type {Object} */
    this.persona = persona

    /** @type {string} */
    this.name = persona.name

    /** @type {string} */
    this.graphic = persona.graphic

    /** @type {Iterator} */
    this.taunts = new Set(persona.taunts).values()

    /** @type {number} */
    this.hp = attributes.hp

    /** @type {number} */
    this.maxHP = attributes.hp
  }

  get level () {
    const level = Math.floor(this.maxHP / 100 / 1.5) || 1

    return level > 20 ? 20 : level
  }

  /**
   * Damage the enemy
   * @param   {number} amount - Amount of damage to take
   * @returns {Enemy} `Enemy` instance
   */
  damage (amount) {
    this.hp -= amount

    if (this.hp < 0) {
      this.hp = 0
    }

    return this
  }

  taunt () {
    let message = this.taunts.next()

    if (message.done) {
      this.taunts = new Set(this.persona.taunts).values()
      message = this.taunts.next()
    }

    return message.value
  }
}

Enemy.setTheme('fantasy')

export default Enemy
