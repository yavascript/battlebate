/* eslint-env jest */

'use strict'

import startCase from 'lodash/startCase'

import Game from './game.lite'
import mockCB from '../test/mocks/cb'

global.MODE = 'lite'
global.ENV = 'production'

jest.useFakeTimers()

const suite = (theme) => {
  describe('Game', () => {
    let cb
    let game

    beforeEach(() => {
      cb = mockCB()
      cb.settings.show_avatars = 'yes'

      game = new Game(cb).start()

      if (theme) {
        game.setTheme(theme)
      }

      jest.spyOn(game.messenger, 'say')
      jest.spyOn(game.messenger, 'taunt')
      jest.spyOn(game.messenger, 'whisper')
    })

    describe('#addPlayer', () => {
      let player

      beforeEach(() => {
        player = '<user>'
        game.addPlayer(player)
      })

      it('should add the given player to the party', () => (
        expect(game.players.has(player)).toBe(true)
      ))
    })

    describe('#spawnEnemy', () => {
      let previousEnemy
      let announcement

      beforeEach(() => {
        previousEnemy = game.enemy
        game.spawnEnemy(500)

        if (theme === 'Batman') {
          announcement = `${startCase(game.enemy.name)} is wreaking havoc!`
        } else {
          announcement = `The ${startCase(game.enemy.name)} appears!`
        }
      })

      it('should replace the current enemy instance', () => (
        expect(game.enemy).not.toBe(previousEnemy)
      ))

      it('should announce the new enemy', () => (
        expect(game.messenger.say).toHaveBeenCalledWith(announcement)
      ))

      it('should assign a hitpoints value', () => (
        expect(game.enemy.hp).toBe(cb.settings.goal_amount)
      ))
    })

    describe('when an attack roll tip is received', () => {
      let attack

      beforeEach(() => {
        const entrance = { user: '<user>' }
        const tip = { from_user: '<user>', amount: 44, message: '' }

        cb.enter(entrance)

        attack = jest.spyOn(game.players.get('<user>'), 'attack')
        cb.tip(tip)
      })

      it('should trigger an attack from the player', () => (
        expect(attack).toHaveBeenCalledWith(game.enemy, 44)
      ))

      it('should add the amount to `totalDamage`', () => (
        expect(game.totalDamage).toBe(44)
      ))
    })

    describe('when a tip is high enough to insta-kill an enemy', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 1000, message: '' })
      })

      it('should spawn a new enemy', () => (
        expect(game.enemy.hp).toBe(1000)
      ))
    })

    describe('when a tip is overkill', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 1500, message: '' })
      })

      it('should carry over to the next enemy', () => (
        expect(game.enemy.hp).toBe(500)
      ))

      it('should carry over across an enemy life', () => {
        expect(game.totalKills).toBe(1)
        expect(game.enemy.hp).toBe(500)
      })
    })

    describe('when a tip is high enough to overkill multiple enemies', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.tip({ from_user: '<user>', amount: 3250, message: '' })
      })

      it('should carry over across multiple enemy lives', () => {
        expect(game.totalKills).toBe(3)
        expect(game.enemy.hp).toBe(750)
      })
    })

    describe('when a message is received', () => {
      beforeEach(() => {
        jest.spyOn(cb._callbacks, 'onMessage')
        cb.message({ user: '<user>', m: '<message>' })
      })

      it('should fire the `onMessage` callback', () => (
        expect(cb._callbacks.onMessage).toHaveBeenCalled()
      ))
    })

    describe('when a message is received that is not a command', () => {
      let exampleHero
      let message

      beforeEach(() => {
        if (theme === 'Batman') {
          exampleHero = {
            name: 'batman',
            label: 'Batman',
            sprite: ':battlebate_batman_novice_2'
          }
        } else {
          exampleHero = {
            name: 'fighter',
            label: 'Fighter',
            sprite: ':battlebate_fighter_novice'
          }
        }
      })

      describe('when the sender is the broadcaster', () => {
        beforeEach(() => {
          message = cb.message({ user: '<broadcaster>', m: '<message>' })
        })

        it('should not decorate the message', () => (
          expect(message.m).toBe('<message>')
        ))
      })

      describe('when avatars are disabled and the player has chosen a hero', () => {
        beforeEach(() => {
          cb.settings.show_avatars = 'no'
          cb.message({ user: '<user>', m: `/hero ${exampleHero.name}` })
          message = cb.message({ user: '<user>', m: '<message>' })
        })

        it('should not decorate the message', () => (
          expect(message.m).toBe('<message>')
        ))
      })

      describe('when avatars are enabled and the player has chosen a hero', () => {
        beforeEach(() => {
          cb.settings.show_avatars = 'yes'
          cb.message({ user: '<user>', m: `/hero ${exampleHero.name}` })
          message = cb.message({ user: '<user>', m: '<message>' })
        })

        it('should decorate the message', () => (
          expect(message.m).toBe(`:: ${exampleHero.sprite} :: <message>`)
        ))
      })
    })

    describe('when a message is received to enable/disable player avatars', () => {
      describe('when the user is a mod', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/avatars off', is_mod: true })
        })

        it('should change the setting', () => (
          expect(cb.settings.show_avatars).toBe('no')
        ))
      })

      describe('when the user is the broadcaster', () => {
        beforeEach(() => {
          cb.message({ user: '<broadcaster>', m: '/avatars off', is_mod: false })
        })

        it('should change the setting', () => (
          expect(cb.settings.show_avatars).toBe('no')
        ))
      })

      describe('when the user is not authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/avatars off', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not change the setting', () => (
          expect(cb.settings.show_avatars).toBe('yes')
        ))
      })

      describe('when a missing or invalid argument is provided', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/avatars', is_mod: true })
        })

        it('should notify the user', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('You dun goofed. Try /avatars on|off', '<user>')
        ))

        it('should not change the setting', () => (
          expect(cb.settings.show_avatars).toBe('yes')
        ))
      })
    })

    describe('when a message is received to set the goal', () => {
      let spawnEnemy

      beforeEach(() => {
        spawnEnemy = jest.spyOn(game, 'spawnEnemy')
      })

      describe('when the token amount is valid', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/goal 1000', is_mod: true })
        })

        it('should spawn a new enemy', () => (
          expect(spawnEnemy).toHaveBeenCalled()
        ))
      })

      describe('when the token amount is invalid', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/goal 0', is_mod: true })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Choose a token amount greater than 0 (e.g. /goal 1000)', '<user>')
        ))

        it('should not spawn an enemy', () => (
          expect(spawnEnemy).not.toHaveBeenCalled()
        ))
      })

      describe('when the user is not a authorized', () => {
        beforeEach(() => {
          cb.message({ user: '<user>', m: '/goal 0', is_mod: false })
        })

        it('should notify the user of an error', () => (
          expect(game.messenger.whisper).toHaveBeenCalledWith('Only the broadcaster or mods can do that', '<user>')
        ))

        it('should not have spawned an enemy', () => (
          expect(spawnEnemy).not.toHaveBeenCalled()
        ))
      })
    })

    describe('when a returning player enters', () => {
      beforeEach(() => {
        cb.enter({ user: '<user>' })
        cb.enter({ user: '<user>' })
      })
    })
  })
}

suite()

export default suite
