/**
 * @overview Provides game state information and user help
 */

'use strict'

import distanceInWordsStrict from 'date-fns/distance_in_words_strict'
import differenceInMinutes from 'date-fns/difference_in_minutes'
import startCase from 'lodash/startCase'

import Graphics from './graphics'
import Player from './player'

function heroTips (game, graphics) {
  const tips = []

  for (const [name, entry] of Player.heroes.entries()) {
    const graphic = Graphics.render(entry.graphics.get('novice'))
    const bonusPrize = entry.bonusPrize

    tips.push(`${graphic} /hero ${name} | Level Up to win: [${game.cb.settings[bonusPrize] || 'random'}]`)
  }

  return tips.join('\n')
}

function showXP (game, player) {
  const maxXP = game.cb.settings.level_up_amount
  const progress = player.xp - (maxXP * (player.level - 1))
  const prize = game.cb.settings[player.hero.bonusPrize] || 'random prize'

  return `${progress} / ${maxXP} XP :: Tip ${maxXP - progress} more to win [${prize}]`
}

function showHeroes (game, graphics) {
  const lines = heroTips(game, graphics)

  return `
    ::: Tip to gain experience points and Level Up! :::
    ${lines}
  `.trim()
}

function showTLDR (game) {
  const lines = [
    '==== Battlebate ====',
    game.announcements.hint()
  ]

  if (game.cb.settings.prize_rolling === 'on') {
    lines.push(`*** Tips of ${game.cb.settings.attack_cost}+ will roll dice for prizes! ***`)
  }

  return lines.join('\n')
}

function showPrizes (game) {
  const ultPrizes = []
  const gamePrizes = game.prizes.map((prize, index) => (
    `${Graphics.render(`die-${index + 1}`)} ${prize}`)
  ).join('\n')

  for (const [name, entry] of Player.heroes.entries()) {
    const bonusPrize = entry.bonusPrize
    const graphic = Graphics.render(entry.graphics.get('novice'))

    ultPrizes.push(`${graphic} ${startCase(name)}: ${game.cb.settings[bonusPrize] || 'random'} | Type /hero ${name}`)
  }

  if (game.cb.settings.prize_rolling === 'on') {
    return `
      ::: Dice Roll Prizes - ${game.cb.settings.attack_cost} tkns to roll :::
      ${gamePrizes}
      ::: Level Up Prizes - Awarded every ${game.cb.settings.level_up_amount} tkns :::
      ${ultPrizes.join('\n')}
    `.trim()
  }

  return `
    ::: Level Up Prizes - Awarded every ${game.cb.settings.level_up_amount} tkns :::
    ${ultPrizes.map(p => `* ${p}`).join('\n')}
  `.trim()
}

function showWinners (game, amount) {
  const now = new Date()

  const winners = game.winners.filter((winning) => (
    differenceInMinutes(now, winning.time) <= 15
  )).slice(-amount)

  const winnersList = winners.map((winning) => (
    `* (${distanceInWordsStrict(now, winning.time, { addSuffix: true })}) ${winning.player.name} won [${winning.prize}]`
  )).join('\n')

  return winners.length
    ? `::: Recent Winners ::: \n ${winnersList}`
    : ''
}

function showHelp (game, graphics) {
  const exampleHero = Player.heroes.values().next().value.name

  return `
    ::: Player Commands :::
    * /hero - Change your Hero and Level Up prize, e.g. /hero ${exampleHero}
    * /prizes - Show all the different prizes
    * /help - Show this help menu

    ::: Broadcaster & Mod Commands :::
    * /avatars on|off - Enable/disable player avatars
    * /dicerolls [on|off] - Turn dice rolls for prizes on or off, e.g. /dicerolls off
    * /goal [amount] - Start a goal of the specified amount of tokens, e.g. /goal 1000
    * /winners - Show recent prize winnners

    ::: Heroes :::
    ${heroTips(game, graphics)}
  `.trim()
}

class Info {
  /**
   * @param {Messenger} messenger - Injected `Messenger` dependency
   * @param {Game} game - Injected `Game` dependency
   * @param {Graphics} graphics - Injected `Graphics` dependency
   */
  constructor (messenger, game, graphics) {
    /** @type {Messenger} */
    this.messenger = messenger

    /** @type {Game} */
    this.game = game

    /** @type {Graphics} */
    this.graphics = graphics
  }

  /**
   * Show current Level Up progress
   * @param {Player} player - Game participant to message
   * @returns {Info} `Info` instance
   */
  showXP (player) {
    this.messenger.whisper(showXP(this.game, player), player.name)

    return this
  }

  /**
   * Show all possible prizes
   * @param {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showPrizes (player) {
    if (player) {
      this.messenger.whisper(showPrizes(this.game), player.name)
    } else {
      this.messenger.say(showPrizes(this.game))
    }

    return this
  }

  /**
   * Show prize winner and time information
   * @param {number} Number of winners to show
   * @returns {Info} `Info` instance
   */
  showWinners (amount = 15) {
    const text = showWinners(this.game, amount)

    if (text) {
      this.messenger.say(text)
    }

    return this
  }

  /**
   * Show information about heroes
   * @param {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showHeroes (player) {
    if (player) {
      this.messenger.whisper(showHeroes(this.game, this.graphics), player.name)
    } else {
      this.messenger.say(showHeroes(this.game, this.graphics))
    }

    return this
  }

  /**
   * Show the help menu to a given user
   * @param {Player} player - Game participant to message
   * @returns {Info} `Info` instance
   */
  showHelp (player) {
    this.messenger.whisper(showHelp(this.game, this.graphics), player.name)

    return this
  }

  /**
   * Show shorter version of help menu
   * @param {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showTLDR (player) {
    if (player) {
      this.messenger.whisper(showTLDR(this.game, this.graphics), player.name)
    } else {
      this.messenger.say(showTLDR(this.game, this.graphics))
    }

    return this
  }
}

export {
  showHeroes,
  showPrizes,
  showWinners,
  showHelp,
  showTLDR
}

export default Info
