## Classes

<dl>
<dt><a href="#Command">Command</a></dt>
<dd></dd>
<dt><a href="#Enemy">Enemy</a></dt>
<dd></dd>
<dt><a href="#Enemy">Enemy</a></dt>
<dd></dd>
<dt><a href="#Game">Game</a></dt>
<dd></dd>
<dt><a href="#Game">Game</a></dt>
<dd></dd>
<dt><a href="#Game">Game</a></dt>
<dd></dd>
<dt><a href="#Game">Game</a></dt>
<dd></dd>
<dt><a href="#Info">Info</a></dt>
<dd></dd>
<dt><a href="#Messenger">Messenger</a></dt>
<dd></dd>
<dt><a href="#Player">Player</a></dt>
<dd></dd>
<dt><a href="#Player">Player</a></dt>
<dd></dd>
</dl>

## Constants

<dl>
<dt><a href="#Registry">Registry</a> : <code>Map.&lt;string, Object&gt;</code></dt>
<dd><p>All known graphics for the game</p>
</dd>
<dt><a href="#Enemies">Enemies</a> : <code>Set</code></dt>
<dd><p>Batman villains</p>
</dd>
<dt><a href="#Enemies">Enemies</a> : <code>Map</code></dt>
<dd><p>Fantasy genre enemies</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#attack">attack(player, damage)</a> ⇒ <code>undefined</code></dt>
<dd><p>Execute an attack against an enemy by a player</p>
</dd>
<dt><a href="#handleEnter">handleEnter(entrance)</a> ⇒ <code>undefined</code></dt>
<dd><p>Handle users entering</p>
</dd>
<dt><a href="#handleMessage">handleMessage(message)</a> ⇒ <code>Object</code></dt>
<dd><p>Handle chat messages</p>
</dd>
<dt><a href="#handleTip">handleTip(tip)</a> ⇒ <code>undefined</code></dt>
<dd><p>Handle chat tips</p>
</dd>
<dt><a href="#attack">attack(player, damage)</a> ⇒ <code>undefined</code></dt>
<dd><p>Execute an attack against an enemy by a player</p>
</dd>
<dt><a href="#handleEnter">handleEnter(entrance)</a> ⇒ <code>undefined</code></dt>
<dd><p>Handle users entering</p>
</dd>
<dt><a href="#handleMessage">handleMessage(message)</a> ⇒ <code>Object</code></dt>
<dd><p>Handle chat messages</p>
</dd>
<dt><a href="#handleTip">handleTip(tip)</a> ⇒ <code>undefined</code></dt>
<dd><p>Handle chat tips</p>
</dd>
</dl>

<a name="Command"></a>

## Command
**Kind**: global class  

* [Command](#Command)
    * [new Command(options)](#new_Command_new)
    * [.registry](#Command+registry) : <code>Map</code>
    * [.exactMatching](#Command+exactMatching) : <code>boolean</code>

<a name="new_Command_new"></a>

### new Command(options)

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Command eval options |
| options.exactMatching | <code>boolean</code> | Whether or not inputs must strictly match command patterns |

<a name="Command+registry"></a>

### command.registry : <code>Map</code>
**Kind**: instance property of [<code>Command</code>](#Command)  
<a name="Command+exactMatching"></a>

### command.exactMatching : <code>boolean</code>
**Kind**: instance property of [<code>Command</code>](#Command)  
<a name="Enemy"></a>

## Enemy
**Kind**: global class  

* [Enemy](#Enemy)
    * [new Enemy(attributes)](#new_Enemy_new)
    * _instance_
        * [.persona](#Enemy+persona) : <code>Object</code>
        * [.name](#Enemy+name) : <code>string</code>
        * [.graphic](#Enemy+graphic) : <code>string</code>
        * [.taunts](#Enemy+taunts) : <code>Iterator</code>
        * [.hp](#Enemy+hp) : <code>number</code>
        * [.maxHP](#Enemy+maxHP) : <code>number</code>
        * [.damage(amount)](#Enemy+damage) ⇒ [<code>Enemy</code>](#Enemy)
    * _static_
        * [.setTheme(theme)](#Enemy.setTheme)
        * [.generate()](#Enemy.generate) ⇒ [<code>Enemy</code>](#Enemy)

<a name="new_Enemy_new"></a>

### new Enemy(attributes)

| Param | Type | Description |
| --- | --- | --- |
| attributes | <code>Object</code> | Initial enemy properties |
| attributes.name | <code>string</code> | Name of the enemy |
| attributes.emoji | <code>string</code> | Icon/visual |
| attributes.hp | <code>number</code> | Total hitpoints |

<a name="Enemy+persona"></a>

### enemy.persona : <code>Object</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+name"></a>

### enemy.name : <code>string</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+graphic"></a>

### enemy.graphic : <code>string</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+taunts"></a>

### enemy.taunts : <code>Iterator</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+hp"></a>

### enemy.hp : <code>number</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+maxHP"></a>

### enemy.maxHP : <code>number</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+damage"></a>

### enemy.damage(amount) ⇒ [<code>Enemy</code>](#Enemy)
Damage the enemy

**Kind**: instance method of [<code>Enemy</code>](#Enemy)  
**Returns**: [<code>Enemy</code>](#Enemy) - `Enemy` instance  

| Param | Type | Description |
| --- | --- | --- |
| amount | <code>number</code> | Amount of damage to take |

<a name="Enemy.setTheme"></a>

### Enemy.setTheme(theme)
Sets theme for available heroes

**Kind**: static method of [<code>Enemy</code>](#Enemy)  

| Param | Type | Description |
| --- | --- | --- |
| theme | <code>string</code> | Name of the theme to use |

<a name="Enemy.generate"></a>

### Enemy.generate() ⇒ [<code>Enemy</code>](#Enemy)
Generate a new enemy

**Kind**: static method of [<code>Enemy</code>](#Enemy)  
**Returns**: [<code>Enemy</code>](#Enemy) - Chosen enemy persona  
<a name="Enemy"></a>

## Enemy
**Kind**: global class  

* [Enemy](#Enemy)
    * [new Enemy(attributes)](#new_Enemy_new)
    * _instance_
        * [.persona](#Enemy+persona) : <code>Object</code>
        * [.name](#Enemy+name) : <code>string</code>
        * [.graphic](#Enemy+graphic) : <code>string</code>
        * [.taunts](#Enemy+taunts) : <code>Iterator</code>
        * [.hp](#Enemy+hp) : <code>number</code>
        * [.maxHP](#Enemy+maxHP) : <code>number</code>
        * [.damage(amount)](#Enemy+damage) ⇒ [<code>Enemy</code>](#Enemy)
    * _static_
        * [.setTheme(theme)](#Enemy.setTheme)
        * [.generate()](#Enemy.generate) ⇒ [<code>Enemy</code>](#Enemy)

<a name="new_Enemy_new"></a>

### new Enemy(attributes)

| Param | Type | Description |
| --- | --- | --- |
| attributes | <code>Object</code> | Initial enemy properties |
| attributes.name | <code>string</code> | Name of the enemy |
| attributes.emoji | <code>string</code> | Icon/visual |
| attributes.hp | <code>number</code> | Total hitpoints |

<a name="Enemy+persona"></a>

### enemy.persona : <code>Object</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+name"></a>

### enemy.name : <code>string</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+graphic"></a>

### enemy.graphic : <code>string</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+taunts"></a>

### enemy.taunts : <code>Iterator</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+hp"></a>

### enemy.hp : <code>number</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+maxHP"></a>

### enemy.maxHP : <code>number</code>
**Kind**: instance property of [<code>Enemy</code>](#Enemy)  
<a name="Enemy+damage"></a>

### enemy.damage(amount) ⇒ [<code>Enemy</code>](#Enemy)
Damage the enemy

**Kind**: instance method of [<code>Enemy</code>](#Enemy)  
**Returns**: [<code>Enemy</code>](#Enemy) - `Enemy` instance  

| Param | Type | Description |
| --- | --- | --- |
| amount | <code>number</code> | Amount of damage to take |

<a name="Enemy.setTheme"></a>

### Enemy.setTheme(theme)
Sets theme for available heroes

**Kind**: static method of [<code>Enemy</code>](#Enemy)  

| Param | Type | Description |
| --- | --- | --- |
| theme | <code>string</code> | Name of the theme to use |

<a name="Enemy.generate"></a>

### Enemy.generate() ⇒ [<code>Enemy</code>](#Enemy)
Generate a new enemy

**Kind**: static method of [<code>Enemy</code>](#Enemy)  
**Returns**: [<code>Enemy</code>](#Enemy) - Chosen enemy persona  
<a name="Game"></a>

## Game
**Kind**: global class  

* [Game](#Game)
    * [new Game(cb)](#new_Game_new)
    * [new Game(cb)](#new_Game_new)
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.prizes](#Game+prizes) : <code>Array</code>
    * [.winners](#Game+winners) : <code>Array.&lt;Object&gt;</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.prizesWon](#Game+prizesWon) : <code>Array</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.setupPrizes()](#Game+setupPrizes) ⇒ [<code>Game</code>](#Game)
    * [.logWinning(player, prize)](#Game+logWinning) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizes"></a>

### game.prizes : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+winners"></a>

### game.winners : <code>Array.&lt;Object&gt;</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizesWon"></a>

### game.prizesWon : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+setupPrizes"></a>

### game.setupPrizes() ⇒ [<code>Game</code>](#Game)
Setup game prizes

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+logWinning"></a>

### game.logWinning(player, prize) ⇒ [<code>Game</code>](#Game)
Logs winnings

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Object</code> | Player that won the prize |
| prize | <code>string</code> | Description of the prize won |

<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game"></a>

## Game
**Kind**: global class  

* [Game](#Game)
    * [new Game(cb)](#new_Game_new)
    * [new Game(cb)](#new_Game_new)
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.prizes](#Game+prizes) : <code>Array</code>
    * [.winners](#Game+winners) : <code>Array.&lt;Object&gt;</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.prizesWon](#Game+prizesWon) : <code>Array</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.setupPrizes()](#Game+setupPrizes) ⇒ [<code>Game</code>](#Game)
    * [.logWinning(player, prize)](#Game+logWinning) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizes"></a>

### game.prizes : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+winners"></a>

### game.winners : <code>Array.&lt;Object&gt;</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizesWon"></a>

### game.prizesWon : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+setupPrizes"></a>

### game.setupPrizes() ⇒ [<code>Game</code>](#Game)
Setup game prizes

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+logWinning"></a>

### game.logWinning(player, prize) ⇒ [<code>Game</code>](#Game)
Logs winnings

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Object</code> | Player that won the prize |
| prize | <code>string</code> | Description of the prize won |

<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game"></a>

## Game
**Kind**: global class  

* [Game](#Game)
    * [new Game(cb)](#new_Game_new)
    * [new Game(cb)](#new_Game_new)
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.prizes](#Game+prizes) : <code>Array</code>
    * [.winners](#Game+winners) : <code>Array.&lt;Object&gt;</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.prizesWon](#Game+prizesWon) : <code>Array</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.setupPrizes()](#Game+setupPrizes) ⇒ [<code>Game</code>](#Game)
    * [.logWinning(player, prize)](#Game+logWinning) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizes"></a>

### game.prizes : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+winners"></a>

### game.winners : <code>Array.&lt;Object&gt;</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizesWon"></a>

### game.prizesWon : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+setupPrizes"></a>

### game.setupPrizes() ⇒ [<code>Game</code>](#Game)
Setup game prizes

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+logWinning"></a>

### game.logWinning(player, prize) ⇒ [<code>Game</code>](#Game)
Logs winnings

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Object</code> | Player that won the prize |
| prize | <code>string</code> | Description of the prize won |

<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game"></a>

## Game
**Kind**: global class  

* [Game](#Game)
    * [new Game(cb)](#new_Game_new)
    * [new Game(cb)](#new_Game_new)
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.prizes](#Game+prizes) : <code>Array</code>
    * [.winners](#Game+winners) : <code>Array.&lt;Object&gt;</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.prizesWon](#Game+prizesWon) : <code>Array</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : [<code>Enemy</code>](#Enemy) \| <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.command](#Game+command) : [<code>Command</code>](#Command)
    * [.messenger](#Game+messenger) : [<code>Messenger</code>](#Messenger)
    * [.info](#Game+info) : [<code>Info</code>](#Info)
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.panel](#Game+panel) : <code>Panel</code>
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.setupPrizes()](#Game+setupPrizes) ⇒ [<code>Game</code>](#Game)
    * [.logWinning(player, prize)](#Game+logWinning) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)
    * [.spawnEnemy()](#Game+spawnEnemy) ⇒ [<code>Game</code>](#Game)
    * [.fetchPlayer(user)](#Game+fetchPlayer) ⇒ [<code>Player</code>](#Player)
    * [.addPlayer(user)](#Game+addPlayer) ⇒ [<code>Player</code>](#Player)
    * [.start()](#Game+start) ⇒ [<code>Game</code>](#Game)

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizes"></a>

### game.prizes : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+winners"></a>

### game.winners : <code>Array.&lt;Object&gt;</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+prizesWon"></a>

### game.prizesWon : <code>Array</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+enemy"></a>

### game.enemy : [<code>Enemy</code>](#Enemy) \| <code>null</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+command"></a>

### game.command : [<code>Command</code>](#Command)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+messenger"></a>

### game.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+info"></a>

### game.info : [<code>Info</code>](#Info)
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+panel"></a>

### game.panel : <code>Panel</code>
**Kind**: instance property of [<code>Game</code>](#Game)  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+setupPrizes"></a>

### game.setupPrizes() ⇒ [<code>Game</code>](#Game)
Setup game prizes

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+logWinning"></a>

### game.logWinning(player, prize) ⇒ [<code>Game</code>](#Game)
Logs winnings

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Object</code> | Player that won the prize |
| prize | <code>string</code> | Description of the prize won |

<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Game+spawnEnemy"></a>

### game.spawnEnemy() ⇒ [<code>Game</code>](#Game)
Replaces current enemy with a newly generated one

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - (self)  
<a name="Game+fetchPlayer"></a>

### game.fetchPlayer(user) ⇒ [<code>Player</code>](#Player)
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="Game+addPlayer"></a>

### game.addPlayer(user) ⇒ [<code>Player</code>](#Player)
Add a given player to the game, by name

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Player</code>](#Player) - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |

<a name="Game+start"></a>

### game.start() ⇒ [<code>Game</code>](#Game)
Start the game, binding event handlers

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: [<code>Game</code>](#Game) - `Game` instance  
<a name="Info"></a>

## Info
**Kind**: global class  

* [Info](#Info)
    * [new Info(messenger, game, graphics)](#new_Info_new)
    * [.messenger](#Info+messenger) : [<code>Messenger</code>](#Messenger)
    * [.game](#Info+game) : [<code>Game</code>](#Game)
    * [.graphics](#Info+graphics) : <code>Graphics</code>
    * [.showXP(player)](#Info+showXP) ⇒ [<code>Info</code>](#Info)
    * [.showPrizes([player])](#Info+showPrizes) ⇒ [<code>Info</code>](#Info)
    * [.showWinners(Number)](#Info+showWinners) ⇒ [<code>Info</code>](#Info)
    * [.showHeroes([player])](#Info+showHeroes) ⇒ [<code>Info</code>](#Info)
    * [.showHelp(player)](#Info+showHelp) ⇒ [<code>Info</code>](#Info)
    * [.showTLDR([player])](#Info+showTLDR) ⇒ [<code>Info</code>](#Info)

<a name="new_Info_new"></a>

### new Info(messenger, game, graphics)

| Param | Type | Description |
| --- | --- | --- |
| messenger | [<code>Messenger</code>](#Messenger) | Injected `Messenger` dependency |
| game | [<code>Game</code>](#Game) | Injected `Game` dependency |
| graphics | <code>Graphics</code> | Injected `Graphics` dependency |

<a name="Info+messenger"></a>

### info.messenger : [<code>Messenger</code>](#Messenger)
**Kind**: instance property of [<code>Info</code>](#Info)  
<a name="Info+game"></a>

### info.game : [<code>Game</code>](#Game)
**Kind**: instance property of [<code>Info</code>](#Info)  
<a name="Info+graphics"></a>

### info.graphics : <code>Graphics</code>
**Kind**: instance property of [<code>Info</code>](#Info)  
<a name="Info+showXP"></a>

### info.showXP(player) ⇒ [<code>Info</code>](#Info)
Show current Level Up progress

**Kind**: instance method of [<code>Info</code>](#Info)  
**Returns**: [<code>Info</code>](#Info) - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| player | [<code>Player</code>](#Player) | Game participant to message |

<a name="Info+showPrizes"></a>

### info.showPrizes([player]) ⇒ [<code>Info</code>](#Info)
Show all possible prizes

**Kind**: instance method of [<code>Info</code>](#Info)  
**Returns**: [<code>Info</code>](#Info) - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | [<code>Player</code>](#Player) | Game participant to message |

<a name="Info+showWinners"></a>

### info.showWinners(Number) ⇒ [<code>Info</code>](#Info)
Show prize winner and time information

**Kind**: instance method of [<code>Info</code>](#Info)  
**Returns**: [<code>Info</code>](#Info) - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| Number | <code>number</code> | of winners to show |

<a name="Info+showHeroes"></a>

### info.showHeroes([player]) ⇒ [<code>Info</code>](#Info)
Show information about heroes

**Kind**: instance method of [<code>Info</code>](#Info)  
**Returns**: [<code>Info</code>](#Info) - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | [<code>Player</code>](#Player) | Game participant to message |

<a name="Info+showHelp"></a>

### info.showHelp(player) ⇒ [<code>Info</code>](#Info)
Show the help menu to a given user

**Kind**: instance method of [<code>Info</code>](#Info)  
**Returns**: [<code>Info</code>](#Info) - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| player | [<code>Player</code>](#Player) | Game participant to message |

<a name="Info+showTLDR"></a>

### info.showTLDR([player]) ⇒ [<code>Info</code>](#Info)
Show shorter version of help menu

**Kind**: instance method of [<code>Info</code>](#Info)  
**Returns**: [<code>Info</code>](#Info) - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | [<code>Player</code>](#Player) | Game participant to message |

<a name="Messenger"></a>

## Messenger
**Kind**: global class  

* [Messenger](#Messenger)
    * [new Messenger(cb)](#new_Messenger_new)
    * [.send(message, options)](#Messenger+send) ⇒ [<code>Messenger</code>](#Messenger)
    * [.say(message)](#Messenger+say) ⇒ [<code>Messenger</code>](#Messenger)
    * [.whisper(message, user)](#Messenger+whisper) ⇒ [<code>Messenger</code>](#Messenger)
    * [.reward(player, prize, die)](#Messenger+reward) ⇒ [<code>Messenger</code>](#Messenger)
    * [.levelUp(player)](#Messenger+levelUp) ⇒ [<code>Messenger</code>](#Messenger)
    * [.taunt(enemy, message)](#Messenger+taunt) ⇒ [<code>Messenger</code>](#Messenger)

<a name="new_Messenger_new"></a>

### new Messenger(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Messenger+send"></a>

### messenger.send(message, options) ⇒ [<code>Messenger</code>](#Messenger)
**Kind**: instance method of [<code>Messenger</code>](#Messenger)  
**Returns**: [<code>Messenger</code>](#Messenger) - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message text |
| options | <code>Object</code> | Output options |
| options.user | <code>string</code> | Username of the message recipient |
| optoins.group | <code>string</code> | Group name of the message recipients |
| options.color | <code>string</code> | Foreground color of the message |

<a name="Messenger+say"></a>

### messenger.say(message) ⇒ [<code>Messenger</code>](#Messenger)
Send a message to all

**Kind**: instance method of [<code>Messenger</code>](#Messenger)  
**Returns**: [<code>Messenger</code>](#Messenger) - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message text |

<a name="Messenger+whisper"></a>

### messenger.whisper(message, user) ⇒ [<code>Messenger</code>](#Messenger)
Send message to a specific user

**Kind**: instance method of [<code>Messenger</code>](#Messenger)  
**Returns**: [<code>Messenger</code>](#Messenger) - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message text |
| user | <code>string</code> | Username of the message recipient |

<a name="Messenger+reward"></a>

### messenger.reward(player, prize, die) ⇒ [<code>Messenger</code>](#Messenger)
Send a message to all, for prize winnings

**Kind**: instance method of [<code>Messenger</code>](#Messenger)  
**Returns**: [<code>Messenger</code>](#Messenger) - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Payer</code> | Message recipient |
| prize | <code>string</code> | Prize to be awarded |
| die | <code>string</code> | Die number of the roll |

<a name="Messenger+levelUp"></a>

### messenger.levelUp(player) ⇒ [<code>Messenger</code>](#Messenger)
Send a message to all, for when a player levels up

**Kind**: instance method of [<code>Messenger</code>](#Messenger)  
**Returns**: [<code>Messenger</code>](#Messenger) - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| player | [<code>Player</code>](#Player) | Player that's leveling up |

<a name="Messenger+taunt"></a>

### messenger.taunt(enemy, message) ⇒ [<code>Messenger</code>](#Messenger)
Send a message to all, for when a player levels up

**Kind**: instance method of [<code>Messenger</code>](#Messenger)  
**Returns**: [<code>Messenger</code>](#Messenger) - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| enemy | [<code>Enemy</code>](#Enemy) | Taunting game enemy |
| message | <code>string</code> | Message text |

<a name="Player"></a>

## Player
**Kind**: global class  

* [Player](#Player)
    * [new Player(name, singleHero)](#new_Player_new)
    * _instance_
        * [.name](#Player+name) : <code>string</code>
        * [.xp](#Player+xp) : <code>number</code>
        * [.hero](#Player+hero) : <code>Object</code>
        * [.emoji](#Player+emoji)
        * [.level](#Player+level)
        * [.changeHero(selectedClass)](#Player+changeHero) ⇒ <code>Object</code>
        * [.attack(enemy, damage)](#Player+attack) ⇒ <code>Object</code>
    * _static_
        * [.configure(setting, value)](#Player.configure)
        * [.setTheme(theme)](#Player.setTheme)

<a name="new_Player_new"></a>

### new Player(name, singleHero)

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | Username of the player |
| singleHero | <code>string</code> | <code>false</code> | Restrict to a single hero, instead of random |

<a name="Player+name"></a>

### player.name : <code>string</code>
**Kind**: instance property of [<code>Player</code>](#Player)  
<a name="Player+xp"></a>

### player.xp : <code>number</code>
**Kind**: instance property of [<code>Player</code>](#Player)  
<a name="Player+hero"></a>

### player.hero : <code>Object</code>
**Kind**: instance property of [<code>Player</code>](#Player)  
<a name="Player+emoji"></a>

### player.emoji
Renders an emoji/avatar for the player

**Kind**: instance property of [<code>Player</code>](#Player)  
**Read only**: true  
<a name="Player+level"></a>

### player.level
Calculates player level based on current XP and the level up requirement

**Kind**: instance property of [<code>Player</code>](#Player)  
**Read only**: true  
<a name="Player+changeHero"></a>

### player.changeHero(selectedClass) ⇒ <code>Object</code>
Change the character class of the player

**Kind**: instance method of [<code>Player</code>](#Player)  
**Returns**: <code>Object</code> - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| selectedClass | <code>string</code> | Selected character class |

<a name="Player+attack"></a>

### player.attack(enemy, damage) ⇒ <code>Object</code>
Attack a given enemy

**Kind**: instance method of [<code>Player</code>](#Player)  
**Returns**: <code>Object</code> - Attack results  

| Param | Type | Description |
| --- | --- | --- |
| enemy | [<code>Enemy</code>](#Enemy) | The enemy to attack |
| damage | <code>number</code> | Amount of damage to apply |

<a name="Player.configure"></a>

### Player.configure(setting, value)
Sets a static `Player` setting

**Kind**: static method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| setting | <code>string</code> | Identifier of setting |
| value | <code>\*</code> | Value to set |

<a name="Player.setTheme"></a>

### Player.setTheme(theme)
Sets theme for available heroes

**Kind**: static method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| theme | <code>string</code> | Name of the theme to use |

<a name="Player"></a>

## Player
**Kind**: global class  

* [Player](#Player)
    * [new Player(name, singleHero)](#new_Player_new)
    * _instance_
        * [.name](#Player+name) : <code>string</code>
        * [.xp](#Player+xp) : <code>number</code>
        * [.hero](#Player+hero) : <code>Object</code>
        * [.emoji](#Player+emoji)
        * [.level](#Player+level)
        * [.changeHero(selectedClass)](#Player+changeHero) ⇒ <code>Object</code>
        * [.attack(enemy, damage)](#Player+attack) ⇒ <code>Object</code>
    * _static_
        * [.configure(setting, value)](#Player.configure)
        * [.setTheme(theme)](#Player.setTheme)

<a name="new_Player_new"></a>

### new Player(name, singleHero)

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | Username of the player |
| singleHero | <code>string</code> | <code>false</code> | Restrict to a single hero, instead of random |

<a name="Player+name"></a>

### player.name : <code>string</code>
**Kind**: instance property of [<code>Player</code>](#Player)  
<a name="Player+xp"></a>

### player.xp : <code>number</code>
**Kind**: instance property of [<code>Player</code>](#Player)  
<a name="Player+hero"></a>

### player.hero : <code>Object</code>
**Kind**: instance property of [<code>Player</code>](#Player)  
<a name="Player+emoji"></a>

### player.emoji
Renders an emoji/avatar for the player

**Kind**: instance property of [<code>Player</code>](#Player)  
**Read only**: true  
<a name="Player+level"></a>

### player.level
Calculates player level based on current XP and the level up requirement

**Kind**: instance property of [<code>Player</code>](#Player)  
**Read only**: true  
<a name="Player+changeHero"></a>

### player.changeHero(selectedClass) ⇒ <code>Object</code>
Change the character class of the player

**Kind**: instance method of [<code>Player</code>](#Player)  
**Returns**: <code>Object</code> - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| selectedClass | <code>string</code> | Selected character class |

<a name="Player+attack"></a>

### player.attack(enemy, damage) ⇒ <code>Object</code>
Attack a given enemy

**Kind**: instance method of [<code>Player</code>](#Player)  
**Returns**: <code>Object</code> - Attack results  

| Param | Type | Description |
| --- | --- | --- |
| enemy | [<code>Enemy</code>](#Enemy) | The enemy to attack |
| damage | <code>number</code> | Amount of damage to apply |

<a name="Player.configure"></a>

### Player.configure(setting, value)
Sets a static `Player` setting

**Kind**: static method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| setting | <code>string</code> | Identifier of setting |
| value | <code>\*</code> | Value to set |

<a name="Player.setTheme"></a>

### Player.setTheme(theme)
Sets theme for available heroes

**Kind**: static method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| theme | <code>string</code> | Name of the theme to use |

<a name="Registry"></a>

## Registry : <code>Map.&lt;string, Object&gt;</code>
All known graphics for the game

**Kind**: global constant  
<a name="Enemies"></a>

## Enemies : <code>Set</code>
Batman villains

**Kind**: global constant  
<a name="Enemies"></a>

## Enemies : <code>Map</code>
Fantasy genre enemies

**Kind**: global constant  
<a name="attack"></a>

## attack(player, damage) ⇒ <code>undefined</code>
Execute an attack against an enemy by a player

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Object</code> | Attacking player |
| damage | <code>number</code> | Damage to apply |

<a name="handleEnter"></a>

## handleEnter(entrance) ⇒ <code>undefined</code>
Handle users entering

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| entrance | <code>string</code> | Chat entrance info |

<a name="handleMessage"></a>

## handleMessage(message) ⇒ <code>Object</code>
Handle chat messages

**Kind**: global function  
**Returns**: <code>Object</code> - Chat message  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>Object</code> | Chat message |

<a name="handleTip"></a>

## handleTip(tip) ⇒ <code>undefined</code>
Handle chat tips

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| tip | <code>Object</code> | Hashmap of tip properties |

<a name="attack"></a>

## attack(player, damage) ⇒ <code>undefined</code>
Execute an attack against an enemy by a player

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| player | <code>Object</code> | Attacking player |
| damage | <code>number</code> | Damage to apply |

<a name="handleEnter"></a>

## handleEnter(entrance) ⇒ <code>undefined</code>
Handle users entering

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| entrance | <code>string</code> | Chat entrance info |

<a name="handleMessage"></a>

## handleMessage(message) ⇒ <code>Object</code>
Handle chat messages

**Kind**: global function  
**Returns**: <code>Object</code> - Chat message  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>Object</code> | Chat message |

<a name="handleTip"></a>

## handleTip(tip) ⇒ <code>undefined</code>
Handle chat tips

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| tip | <code>Object</code> | Hashmap of tip properties |

