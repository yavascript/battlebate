A prize-rolling and goal app for Chaturbate, with a role-playing combat twist.

Players (tippers) select a hero (fighter, warlock, or bard) and "fight" the target goal, represented by a monster or enemy.

Developing:
- Run `npm install` to grab dependencies
- Execute `npm run build` to transpile into `dist/bundle.js` (production build)
- For testing, run `npm test`

## TODO

- [x] [Feature] Remove Critical Hits
- [x] [Enhancement] Add more prize slots
- [x] [Enhancement] Enemy health bar graphics
- [x] [Enhancement] Reduce game text noise
- [x] [Feature] XP bar/reminder
- [ ] Skyrim theme
- [ ] [Enhancement] Instructional text image
- [x] [Enhancement] /winners reminder
- [ ] [Feature] Number of entries argument for `/winners`
- [ ] [Feature] Complete in-game settings config
